<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Schema;
use App\Models\Artikel;
use App\Http\Requests\CreateArtikelRequest;
use App\Http\Requests\UpdateArtikelRequest;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\UserApp;

use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Traits\ImageUpload;
use App\Models\Schedule_Content;
use DateTime;


class ArtikelController extends Controller {

    /**
     * Display a listing of artikel
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $idustad = Auth::user()->id;
        // $category = Auth::user()->user_category ;
        // if($category == 1){
        //     $artikel = Artikel::select('artikel.id as id_artikel','user_app.name','category.nama_category','artikel.judul')
        //         ->with("category")->WHERE('id_ustad','=',$idustad)->orderByRaw('created_at DESC')->get();
        // }
        // else {
            $artikel = Artikel::select('artikel.id as id_artikel','user_app.name','category.nama_category','artikel.judul')->with("category")
                ->JOIN('user_app', 'user_app.id', '=', 'id_ustad')
                ->JOIN('category', 'category.id', '=', 'artikel.category_id')
                ->orderByRaw('artikel.created_at DESC')->get();
        // }
        return view('admin.artikel.index', compact('artikel'));
    }

    /**
     * Show the form for creating a new artikel
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);
        $ustad = UserApp::select('id','name')->WHERE('user_category','=','1')->get();

//        return $ustad;
        return view('admin.artikel.create', compact("category","ustad"));
    }

    /**
     * Store a newly created artikel in storage.
     *
     * @param CreateArtikelRequest|Request $request
     */
    public function store(CreateArtikelRequest $request, ImageUpload $imageUpload, Schedule_Content $schedule)
    {
        $type = 'artikel';
        $request = $imageUpload->saveImage($request, $type);
        $foto = $request->image;
        if(empty($foto)){
            $gambar = "";
        }
        elseif (!empty($foto)){
            $gambar = $foto;
        }
        $post = new Artikel();
        $post->id_ustad = $request->id_ustad;
        $post->category_id     =  $request->category_id;
        $post->judul = $request->judul;
        $post->image = $foto;
        $post->isi_artikel = $request->isi_artikel;
        $post->status = $request->status;

        $post->save();

        $last = $post->id;
//        $last =1 ;
        $title = $request->judul;
        $url = "https://app.haloustadz.id/artikel/".$last;
        $status = $request->status;
        $tanggal = $request->date_publish;
        $tgl_publish = date_format(new DateTime($tanggal), 'Y-m-d H:i:s');
        $datacontent = ['post_categori'=>$type,'id_post'=>$last,'published'=>$tgl_publish,'status'=>$status];
        $content = "<img src='".$gambar."'/><br>".$request->isi_artikel;
        switch ($request->id_ustad) {
            case 81:
                $author = "2";
                break;
            case 82:
                $author = "3";
                break;
            case 83:
                $author = "4";
                break;
            case 84:
                $author = "5";
                break;
            case 85:
                $author = "6";
                break;
            case 86:
                $author = "7";
                break;
            case 87:
                $author = "8";
                break;
            case 88:
                $author = "9";
                break;
            case 89:
                $author = "10";
                break;
            case 90:
                $author = "11";
                break;
            case 91:
                $author = "12";
                break;
            default:
                $author = "1";
        }

        switch ($request->category_id) {
            case 1:
                $category = "52";
                break;
            case 2:
                $category = "53";
                break;
            case 3:
                $category = "54";
                break;
            case 4:
                $category = "55";
                break;
            case 5:
                $category = "56";
                break;
            case 6:
                $category = "57";
                break;
            case 7:
                $category = "58";
                break;
            case 8:
                $category = "59";
                break;
            case 9:
                $category = "60";
                break;

            default:
                $category = "1";
        }

//        $hasil = $this->postwp($title, $content, $author, $category);
//        $myJSON = json_decode($hasil);
//
//        Artikel::where('id', $last)->update(['link' => $myJSON->link]);
        if($status == 1){
            $schedule->adddata($datacontent);
        }
        elseif($status == 0){
            $this->sendMessage($title,$foto,$url);
        }

        return redirect('/content/artikel');
//        return $response_cotent;
    }




    /**
     * Show the form for editing the specified artikel.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $artikel = Artikel::find($id);
        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);
        return view('admin.artikel.edit', compact('artikel', "category"));
    }

    /**
     * Update the specified artikel in storage.
     * @param UpdateArtikelRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateArtikelRequest $request, ImageUpload $imageUpload)
    {
        $artikel = Artikel::findOrFail($id);
        $type = 'artikel';
        $request = $imageUpload->saveImage($request, $type);
        $artikel->update($request->all());
        return redirect()->route('content.artikel.index');
    }

    /**
     * Remove the specified artikel from storage.
     *
     * @param  int  $id
     */
    public function destroy($id, Schedule_Content $schedule)
    {
        Artikel::destroy($id);
        $schedule->deletedata($id,'artikel');
//        return $id;
        return redirect()->route('content.artikel.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Artikel::destroy($toDelete);
        } else {
            Artikel::whereNotNull('id')->delete();
        }

        return redirect()->route('content.artikel.index');
    }
    function sendMessage($pesan, $image, $url){
        $content = array(
            "en" => $pesan

        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'excluded_segments' => array('notification_ustadz_online_unreceived'),
//            'include_player_ids' => ['a0985aaa-e66b-453d-a015-6d116877841a'],
            'data' => array("haloOfTheDayUrlImage" => $image, "haloOfTheDayUrlDetail" => $url, "haloOfTheDayTitle"=>$pesan, "type"=>"haloOfTheDay"),
            'contents' => $content,
            'small_icon' => 'img_halo_logo'
        );

        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);
    }
}

