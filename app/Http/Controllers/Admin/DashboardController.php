<?php


namespace App\Http\Controllers\Admin;

use App\Models\UserApp;
use App\Models\Event;
use App\Http\Controllers\Controller;
//use http\Env\Request;
use function MongoDB\BSON\toJSON;
use ReCaptcha\Response;
use Illuminate\Http\Request;

class DashboardController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $userapp = UserApp::where('user_category','=','1')->count();
        $ustad = UserApp::where('user_category','=','1')->get();
        $jamaah = UserApp::where('user_category','=','2')->count();
        $event  =  Event::all();



		return view('admin.dashboard.index', compact('userapp','event', 'jamaah', 'ustad'));
	}
	public function status(Request $request){
        $id = $request->id;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.haloustadz.id/status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"id\"\r\n\r\nid\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"status\"\r\n\r\n0\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFuYXNidXJoYW51ZGRpbkBoYWxvdXN0YWR6LmlkIiwidXNlcm5hbWUiOiJhbmFzYnVyaGFudWRkaW4iLCJpYXQiOjE1NDc0NzI0NjZ9.bAmT2CEYFusuMRQrcnbKyZpF1YltdulmxY8iAanygzA",
                "Postman-Token: 1a134d20-0255-482a-87b3-c5c2fa4157f3",
                "cache-control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
                "id:".$id,
                "status: 0"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
//            echo "cURL Error #:" . $err;
            return response()->json($err, 200);
        } else {
//
            $jsondecode = json_decode($response, true);
        return $jsondecode["message"];
        }

    }
    private function update_status($id){
//        $id = $request->id;

    }

}