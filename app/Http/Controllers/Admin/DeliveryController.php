<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;


class DeliveryController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $delivery = Order::WHERE('status', '=', '3')->orderBy('nama', 'asc')->get();
		return view('admin.delivery.index',compact('delivery'));
	}

	public function editresi(Request $request){
        $get = Order::find($request->id);
        $get->resi = $request->resi;
        $get->save();
        $data=[
            "message"=>"succes"
        ];
        return response()->json($data, 200);
    }

}