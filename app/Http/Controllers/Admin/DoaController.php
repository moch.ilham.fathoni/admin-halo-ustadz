<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Models\Doa_content;
use App\Models\Doa_type;
use Illuminate\Http\Request;
use App\Models\UserApp;
use Illuminate\Support\Facades\URL;

class DoaController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $Doa_type = Doa_type::all();
        $Doa_content = Doa_content::all();
		return view('admin.doa.index',compact('Doa_content','Doa_type'));
	}

    public function create()
    {
        $Doa_type = Doa_type::all();


        return view('admin.doa.create', compact("Doa_type"));
    }

    public function createcategorydoa(Request $request){

        $post = new Doa_type();
        $post->name = $request->name;
        $post->save();

        return redirect()->route(config('quickadmin.route').'.doa.index');
    }

    public function createdoa($id)
    {
        $Doa_type = Doa_type::WHERE('id','=',$id)->get();
        return view('admin.doa.create', compact("Doa_type"));
//
    }

    /**
     * Store a newly created recording in storage.
     *
     * @param CreateRecordingRequest|Request $request
     */
    public function store(Request $request)
    {

        $post = new Doa_content();
        $post->subtitle = $request->subtitle;
        $post->caption = $request->caption;
        $post->text_arab = $request->text_arab;
        $post->text_latin = $request->text_latin;
        $post->terjemah = $request->terjemah;
        $post->title_id = $request->title_id;
        $post->save();

        return redirect()->route(config('quickadmin.route').'.doa.index');
    }

    /**
     * Show the form for editing the specified recording.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $Doa_type = Doa_type::all();
        $Doa_content = Doa_content::find($id);


        return view('admin.doa.edit', compact('Doa_content', "Doa_type"));
    }

    /**
     * Update the specified recording in storage.
     * @param UpdateRecordingRequest|Request $request
     *
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {

        Doa_content::WHERE('id','=', $id)
            ->update([
                'subtitle' => $request->subtitle,
                'caption' => $request->caption,
                'text_arab' => $request->text_arab,
                'text_latin' => $request->text_latin,
                'terjemah' => $request->terjemah,
                'title_id' => $request->title_id
            ]);
        return redirect()->route(config('quickadmin.route').'.doa.index');
//    return $request;
    }

    /**
     * Remove the specified recording from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Doa_type::destroy($id);

        return redirect()->route(config('quickadmin.route').'.doa.index');
    }

    public function delete($id)
    {
        Doa_content::destroy($id);
        return redirect()->route(config('quickadmin.route').'.doa.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Doa_type::destroy($toDelete);
        } else {
            Doa_type::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.doa.index');
    }

}