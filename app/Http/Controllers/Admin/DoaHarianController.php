<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Doa;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\ImageUpload;
use App\Models\Schedule_Content;
use DateTime;

class DoaHarianController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
    public function index(Doa $doa)
    {
        $data_doa = $doa->getdatadoa();
        return view('admin.doaharian.index',compact('data_doa'));
//        return $data_doa;
    }
    public function store(Doa $doa,  Request $request, ImageUpload $imageUpload,Schedule_Content $content ){
        $type = 'doa';
        $request = $imageUpload->saveImage($request, $type);
        $save = $doa->addnew($request);
        $image =$request->image;
        $status =$request->status;
        $tanggal = $request->date_publish;
        $tgl_publish = date_format(new DateTime($tanggal), 'Y-m-d H:i:s');
        $url = "https://app.haloustadz.id/doa/".$save;
        $pesan = $request->title;
        $datacontent = ['post_categori'=>$type,'id_post'=>$save,'published'=>$tgl_publish,'status'=>$status];
        if($status == 1){
            $response_cotent = $content->adddata($datacontent);
        }
        elseif($status == 0){
            $this->sendMessage($pesan,$image,$url);
        }

        return redirect('/content/doaharian');
//        return $request;
    }
    public function destroy(Doa $doa, $id, Schedule_Content $schedule){
        $doa->deletecontent($id);
        $schedule->deletedata($id,'doa');
        return redirect('/content/doaharian');
    }
    public function edit(Doa $doa, $id){
        $data_doa = $doa->editcontent($id);
        return response()->json($data_doa, 200);
    }
    public function update(Request $request, Doa $doa, $id){
        $doa->updatecontent($id, $request);
    }
    function sendMessage($pesan, $image, $url){
        $content = array(
            "en" => $pesan

        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'excluded_segments' => array('notification_ustadz_online_unreceived'),
//            'include_player_ids' => ['a0985aaa-e66b-453d-a015-6d116877841a'],
            'data' => array("haloOfTheDayUrlImage" => $image, "haloOfTheDayUrlDetail" => $url, "haloOfTheDayTitle"=>$pesan, "type"=>"haloOfTheDay"),
            'contents' => $content,
            'small_icon' => 'img_halo_logo'
        );

        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }

}