<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Support\Facades\DB;

class DuplicateController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
//	public function index()
//    {
//
//		return view('admin.duplicate.index');
//	}
    public function index(Order $order)
    {
        $packing = Order::where('status','=','1')
            ->orWhere('status','=','2')
            ->orWhere('status','=','3')
            ->orderBy('nama', 'asc')
            ->get();
        return view('admin.duplicate.index',compact('packing'));
    }

}