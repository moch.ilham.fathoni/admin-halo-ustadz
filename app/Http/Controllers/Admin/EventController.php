<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Models\Event;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\UpdateEventRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Models\Category;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;



class EventController extends Controller {
    use FileUploadTrait;

	/**
	 * Display a listing of event
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $event = Event::with("category")->get();

		return view('admin.event.index', compact('event'));
	}

	/**
	 * Show the form for creating a new event
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $category = Category::pluck("nama_category", "id")->prepend('Please select', null);

	    return view('admin.event.create', compact("category"));
	}

	/**
	 * Store a newly created event in storage.
	 *
     * @param CreateEventRequest|Request $request
	 */
	public function store(CreateEventRequest $request)
	{
        $request = $this->saveFiles($request);
        $foto = str_replace(' ','_',$request->image);
        $url = URL::to("/public/uploads");
        $image = $url.'/'.$foto;
        if(empty($foto)){
            $gambar = "";
        }
        elseif (!empty($foto)){
            $gambar = $image;
        }
        $post = new Event();
        $post->category_id = $request->category_id;
        $post->nama = $request->nama;
        $post->image = $image;
        $post->deskripsi = $request->deskripsi;
        $post->category_post = $request->category_post;
        $post->save();
        $last = $post->id;
        $title = $request->nama;
        $content = "<img src='".$gambar."'/><br>".$request->deskripsi;

//		Event::create($request->all());
        $url = "https://app.haloustadz.id/info/".$last;
        $pesan = "[".$request->nama."]";

		if($request->ifnotif == 1){
//            $this->sendMessage($pesan);
            $this->SendNotif($pesan,$image, $url);
        }

        if($request->category_post == 1){
            $category = '62';
        }
        else if ($request->category_post == 2){
            $category = '61';
        }
        else{
            $category = '1';
        }
        $hasil = $this->postwp($title, $content, $category);
        $myJSON = json_decode($hasil);

        if ($myJSON) {
            Event::where('id', $last)->update(['link' => $myJSON->link ?? '']);
        }

		return redirect()->route('content.event.index');
	}

    function postwp($title, $content, $category){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://haloustadz.id/wp-json/wp/v2/posts",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>  "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"title\"\r\n\r\n".$title."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n".$content."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"author\"\r\n\r\n14\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"categories\"\r\n\r\n51,".$category."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"status\"\r\n\r\npublish\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic YWRtaW46c2VtYW5nYXQxMjMzMjE=",
                "Cache-Control: no-cache",
                "Postman-Token: 7d4bc8dc-d618-4e99-8892-b6b4ba86be32",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
        }
    }

	/**
	 * Show the form for editing the specified event.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$event = Event::find($id);
	    $category = Category::pluck("nama_category", "id")->prepend('Please select', null);

		return view('admin.event.edit', compact('event', "category"));
	}

	/**
	 * Update the specified event in storage.
     * @param UpdateEventRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateEventRequest $request)
	{
		$event = Event::findOrFail($id);

        $request = $this->saveFiles($request);

		$event->update($request->all());

		return redirect()->route('content.event.index');
	}

	/**
	 * Remove the specified event from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Event::destroy($id);

		return redirect()->route('content.event.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Event::destroy($toDelete);
        } else {
            Event::whereNotNull('id')->delete();
        }

        return redirect()->route('content.event.index');
    }

    public function onesignal(){

        $pesan = "ini pesan";
        $response = $this->sendMessage($pesan);
        $return["allresponses"] = $response;
        $return = json_encode( $return);

//        print("\n\nJSON received:\n");
//        print($return);
//        print("\n");
//        return 1;
    }

    function sendMessage($pesan){
        $content = array(
            "en" => $pesan
        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }
    function SendNotif($pesan, $image, $url){
        $content = array(
            "en" => $pesan

        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'excluded_segments' => array('notification_ustadz_online_unreceived'),
//            'include_player_ids' => ['a0985aaa-e66b-453d-a015-6d116877841a'],
            'data' => array("haloOfTheDayUrlImage" => $image, "haloOfTheDayUrlDetail" => $url, "haloOfTheDayTitle"=>$pesan),
            'contents' => $content
        );

        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }

}
