<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

//use App\Model\Hadits;
use App\Models\Hadits;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\ImageUpload;
use DateTime;
use App\Models\Schedule_Content;



class HaditsController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Hadits $hadits)
    {
        $data_hadits = $hadits->getdatahadits();
//        return $data_hadits;

//        $pesan = 'coba';
//        $response = $this->sendMessage($pesan);
//        $return["allresponses"] = $response;
//        $return = json_encode( $return);
		return view('admin.hadits.index',compact('data_hadits'));

	}
	public function store(Hadits $hadits,  Request $request, ImageUpload $imageUpload, Schedule_Content $schedule){
        $type = 'hadits';
        $request = $imageUpload->saveImage($request, $type);
        $save = $hadits->addnew($request);
        $pesan = $request->judul;
        $image = $request->image;
        $url="https://app.haloustadz.id/hadits/".$save;
        $status = $request->status;
        $tanggal = $request->date_publish;
        $tgl_publish = date_format(new DateTime($tanggal), 'Y-m-d H:i:s');
        $datacontent = ['post_categori'=>$type,'id_post'=>$save,'published'=>$tgl_publish,'status'=>$status];

//        $return["allresponses"] = $response;
//        $return = json_encode( $return);
//        return $hadits->id;
        $title = $request->judul;
        $content = "<img src='".$image."'/><br><h3 style='text-align: right;'>".$request->content_arab."</h3>"."<p>".$request->content_text."</p>";
        $hasil = $this->postwp($title, $content);
        $myJSON = json_decode($hasil);
        if($status == 1){
            $schedule->adddata($datacontent);
        }
        elseif($status == 0){
            $this->sendMessage($pesan, $image, $url);
        }
        return redirect('/admin/hadits');
    }
    function postwp($title, $content){
        $curl = curl_init();
        $author = 14;
        $category = 50;
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://haloustadz.id/wp-json/wp/v2/posts",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>  "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"title\"\r\n\r\n".$title."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content\"\r\n\r\n".$content."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"author\"\r\n\r\n".$author."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"categories\"\r\n\r\n51,".$category."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"status\"\r\n\r\npublish\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic YWRtaW46c2VtYW5nYXQxMjMzMjE=",
                "Cache-Control: no-cache",
                "Postman-Token: 7d4bc8dc-d618-4e99-8892-b6b4ba86be32",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;
//            return;
        }
    }
    public function destroy(Hadits $hadits, $id, Schedule_Content $schedule){
	    $hadits->deletecontent($id);
        $schedule->deletedata($id,'hadits');
	    return redirect('/admin/hadits');
    }
    public function edit(Hadits $hadits, $id){
//        $data_hadits = $hadits->editcontent($id);
        $data_hadits = Hadits::find($id);


//        return response()->json($data_hadits, 200);
        return view('admin.hadits.edit', compact('data_hadits'));

    }
    public function update(Request $request, Hadits $hadits, $id){
        $hadits->updatecontent($id, $request);
    }

//    public function edit(Doa $doa, $id){
//        $data_doa = $doa->editcontent($id);
//        return response()->json($data_doa, 200);
//    }
//    public function update(Request $request, Doa $doa, $id){
//        $doa->updatecontent($id, $request);
//    }

    function onesignal(){


//        print("\n\nJSON received:\n");
//        print($return);
//        print("\n");
//        return 1;
    }

    function sendMessage($pesan, $image, $url){
        $content = array(
            "en" => $pesan

        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'excluded_segments' => array('notification_ustadz_online_unreceived'),
//            'include_player_ids' => ['a0985aaa-e66b-453d-a015-6d116877841a'],
            'data' => array("haloOfTheDayUrlImage" => $image, "haloOfTheDayUrlDetail" => $url, "haloOfTheDayTitle"=>$pesan, "type"=>"haloOfTheDay"),
            'contents' => $content,
            'small_icon' => 'img_halo_logo'
        );

        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }

}
