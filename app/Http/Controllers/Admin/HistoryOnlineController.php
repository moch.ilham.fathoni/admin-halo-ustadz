<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserApp;
use App\Artikel;
use App\History_telepon;
use App\UstadOnline;
use Redirect;
use Schema;
use Illuminate\Http\Request;
use DateTime;

class HistoryOnlineController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $id_ustad = 81;
//        $id_ustad = $request->ustad;
        $starttime = date("Y-m-d "."00:00:00");
        $endtime = date("Y-m-d"." 23:59:00");
        $ustad = UserApp::SELECT('id','name')->WHERE('user_category','1')->get();
//        $jamaah = UserApp::SELECT('id','name')->WHERE('user_category','2')->get();
        $ustadonline = UstadOnline::SELECT('history_ustad.id AS id_h_ustad','user_app.name', 'start_time', 'end_time', 'total_time', 'user_app_id')->JOIN('user_app', 'user_app.id', '=', 'user_app_id')->WHERE('user_app_id','=',$id_ustad)->whereBetween('start_time', [$starttime, $endtime])->orderBy('history_ustad.start_time','ASC')->get
        ();
//        $history_telepon = History_telepon::SELECT('history_telepon.start_time','history_telepon.end_time','record_url','duration','history_ustad_id','user_app.name', 'user_app.tlp')
//            ->JOIN('user_app', 'user_app.id', '=', 'jamaah_id')
//            ->JOIN('history_ustad', 'history_ustad.id', '=', 'history_telepon.history_ustad_id')
//            ->WHERE('history_ustad.user_app_id','=',$id_ustad)
//            ->whereBetween('history_telepon.start_time', [$starttime, $endtime])->get();
//        if ($request->ajax()) {
//            return view('pagination_data', compact('ustad'))->render();
//
//        }
        return view('admin.historyonline.index',compact('ustadonline','ustad'));
    }

    public function getdata(request $request){
        $id_ustad = $request->ustad;
        if(empty($request->starttime)){
            $starttime = date("Y-m-d "."00:00:00");
            $endtime = date("Y-m-d"." 23:59:00");
        }
        else{
            $starttime = $request->starttime;
            $endtime = $request->endtime;
        }

        $ustad = UserApp::SELECT('id','name')->WHERE('user_category','1')->get();
//        $jamaah = UserApp::SELECT('id','name')->WHERE('user_category','2')->get();
        $ustadonline = UstadOnline::SELECT('history_ustad.id AS id_h_ustad','user_app.name', 'start_time', 'end_time', 'total_time', 'user_app_id')->JOIN('user_app', 'user_app.id', '=', 'user_app_id')->WHERE('user_app_id','=',$id_ustad)->whereBetween('start_time', [$starttime, $endtime])->orderBy('history_ustad.start_time','ASC')->get
        ();
//        $history_telepon = History_telepon::SELECT('history_telepon.start_time','history_telepon.end_time','record_url','duration','history_ustad_id','user_app.name', 'user_app.tlp')
//            ->JOIN('user_app', 'user_app.id', '=', 'jamaah_id')
//            ->JOIN('history_ustad', 'history_ustad.id', '=', 'history_telepon.history_ustad_id')
//            ->WHERE('history_ustad.user_app_id','=',$id_ustad)
//            ->whereBetween('history_telepon.start_time', [$starttime, $endtime])->get();
        return view('admin.historyonline.index',compact('ustadonline','ustad'));

    }

    public function rekap(request $request){
        $id_ustad = $request->ustad;
        if(empty($request->starttime)){
            $starttime = date("Y-m-d "."00:00:00");
            $endtime = date("Y-m-d"." 23:59:00");
        }
        else{
            $starttime = $request->starttime;
            $endtime = $request->endtime;
        }

        $ustad = UserApp::SELECT('id','name')->WHERE('user_category','1')->get();
        $ustadonline = UstadOnline::SELECT('history_ustad.id AS id_h_ustad','user_app.name', 'start_time', 'end_time', 'total_time', 'user_app_id')
            ->JOIN('user_app', 'user_app.id', '=', 'user_app_id')
            ->WHERE('user_app_id','=',$id_ustad)
            ->whereBetween('start_time', [$starttime, $endtime])
            ->orderBy('history_ustad.start_time','ASC')->get
        ();
//        $artikel = Artikel::SELECT('id', 'created_at', 'id_ustad', 'judul')->whereBetween('created_at', [$starttime, $endtime])->get();

        return view('admin.historyonline.rekap', compact('ustadonline','ustad'));
    }
    public function get_rekaman(Request $request){
        $id_history = $request->history;

        $history_telepon = History_telepon::SELECT('history_telepon.start_time','history_telepon.end_time','record_url','duration','history_ustad_id','user_app.name', 'user_app.tlp')
            ->JOIN('user_app', 'user_app.id', '=', 'jamaah_id')
            ->JOIN('history_ustad', 'history_ustad.id', '=', 'history_telepon.history_ustad_id')
            ->WHERE('history_ustad_id','=',$id_history)->get();
//        return response()->json($history_telepon, 200);
        return response()->json($history_telepon, 200);

    }

}