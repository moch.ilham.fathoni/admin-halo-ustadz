<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Redirect;
use Schema;
use App\Model\image_praytime;
use App\Http\Controllers\Traits\ImageUpload;


class ImagePrayTimeController extends Controller {
    public function index(image_praytime $image_praytime)
    {
        $praytime = $image_praytime->getcontent();
        return view('admin.imagepraytime.index',compact('praytime'));

    }

    public function edit(image_praytime $image_praytime, $id){
        $praytime = $image_praytime->editcontent($id);
        return view('admin.imagepraytime.edit',compact('praytime'));

    }
    public function update(Request $request, ImageUpload $imageUpload, image_praytime $image_praytime, $id){
        $type = 'imagepraytime';
        $request = $imageUpload->saveImage($request, $type);
        $image_praytime->updatecontent($id, $request);
        return redirect('admin/imagepraytime');
    }



}