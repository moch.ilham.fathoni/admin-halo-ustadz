<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\UserApp;
use App\Schedule;
use App\Http\Requests\CreateUstadRequest;
use App\Http\Requests\UpdateUstadRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Category;

class JamaahController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $schedule = Schedule::all();
        $ustad = UserApp::where('user_category' ,'=', '2')->with("category")->latest('created_at')->paginate(15);
        if ($request->ajax()) {
            return view('pagination_data', compact('ustad'))->render();

        }
//        return
        return view('admin.jamaah.index', compact('ustad','schedule'));
	}

    public function fetch_data(Request $request)
    {

        if($request->ajax())
        {
            $ustad = UserApp::where('user_category' ,'=', '2')->with("category")->latest('created_at')->paginate(15);
            return view('admin.jamaah.load', compact('ustad'))->render();
//            return 1;
        }
    }

    public function searcha(Request $request){


        if($request->ajax())
        {
            $keywoard = $request->get('query');
            $ustad = UserApp::where('user_category' ,'=', '2')
                ->Where('name','LIKE',"%$keywoard%")
                ->orWhere('email','LIKE',"%$keywoard%")
                ->orWhere('username','LIKE',"%$keywoard%")
                ->orWhere('tlp','LIKE',"%$keywoard%")
                ->get();
//            return view('admin.jamaah.load', compact('ustad'))->render();
            return $keywoard."1";
        }

    }

    function search(Request $request)
    {
        if($request->ajax())
        {
            $output = '';
            $query = $request->get('query');
            if($query != '')
            {
                $data = UserApp::where('user_category' ,'=', '2')
                    ->Where('name','=',"%$query%")
                    ->orWhere('email','=',"%$query%")
                    ->orWhere('username','=',"%$query%")
                    ->get();

            }
            else
            {
                $data = UserApp::where('user_category' ,'=', '2')
                    ->Where('name','=',"%$query%")
                    ->orWhere('email','=',"%$query%")
                    ->orWhere('username','=',"%$query%")
                    ->get();
            }
            $total_row = $data->count();
            if($total_row > 0)
            {
                foreach($data as $row)
                {
                    $output .= '
        <tr>
         <td>'.$row->CustomerName.'</td>
         <td>'.$row->Address.'</td>
         <td>'.$row->City.'</td>
         <td>'.$row->PostalCode.'</td>
         <td>'.$row->Country.'</td>
        </tr>
        ';
                }
            }
            else
            {
                $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
            }
            $data = array(
                'table_data'  => $output,
                'total_data'  => $total_row
            );

            echo json_encode($data);
        }
    }

    public function DelFox($userid){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/DelUser/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&user_id=".$userid,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {


            return $response;

        }
    }

    public function ifActive($userid, $status){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/SetUserInfo/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&user_id=$userid&user_active=$status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",

        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {


            return $response;

        }
    }


    /**
     * Show the form for editing the specified ustad.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {

        $ustad = UserApp::find($id);
        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);


        return view('admin.ustad.edit', compact('ustad', "category"));
    }

    /**
     * Update the specified ustad in storage.
     * @param UpdateUstadRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateUstadRequest $request)
    {
        $ustad = UserApp::findOrFail($id);

        $request = $this->saveFiles($request);

        $ustad->update($request->all());

        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

    /**
     * Remove the specified ustad from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {

        $ustad = UserApp::select('vox_id')->where('user_category' ,'=', '2')->where('id','=',$id)->get();
        foreach ($ustad as $row){
            $userid = $row->vox_id;
        }
        $this->DelFox($userid);
        UserApp::destroy($id);
        return redirect()->route(config('quickadmin.route').'.jamaah.index');
//        return $id;

    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            UserApp::destroy($toDelete);
        } else {
            UserApp::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.jamaah.index');
    }

    public function isactive($id, $is_active){


    }

    public function blackList(Request $request){
        $is_active = $request->is_active;
        $id = $request->id;
//        $this->isactive($id, $is_active);
        $id_voxim=UserApp::select('vox_id')->where('id','=',$id)->get();
        foreach ($id_voxim as $vox_id){
            $voxid=$vox_id->vox_id;
        }
        if($is_active == 1 ){
            $status = "false";
        }
        else{
            $status = "true";
        }
//
        $post = UserApp::find($id);
        $post->is_active = $is_active;

        $post->save();
//        $last = dd($post->vox_id);
        $a = $this->ifActive($voxid, $status);
        return redirect()->route(config('quickadmin.route').'.jamaah.index');

//        echo $status;


    }
    public function getemail(Request $request)
    {
        $email = $request->search;
        $schedule = Schedule::all();
        $ustad = UserApp::where('email' ,'like', '%'.$email.'%')->where('user_category' ,'=', '2')->with("category")->latest('created_at')->paginate(15);
        // return $email;
        return view('admin.jamaah.index', compact('ustad','schedule'));
    }

}