<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ImageUpload;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class LogoRekamanController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {

		return view('admin.logorekaman.edit');
	}
	public function edit(){
	    return view('admin.logorekaman.edit');
    }

    public function update(Request $request){
        if (!file_exists(public_path('uploads'))) {
            mkdir(public_path('uploads/file/logorekaman'), 0777);

        }
        $type = 'logorekaman';

                $file = $request->file('image');
                $ext = $file->getClientOriginalExtension();
                $newName = "logo_rekaman." . $ext;
                $location = public_path('uploads/file/' . $type . '/' . $newName);
                Image::make($file)->resize(500, 500, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($location);


        return redirect()->action('Admin\LogoRekamanController@index');
    }

}