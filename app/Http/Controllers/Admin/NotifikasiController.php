<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Redirect;
use Schema;
use Illuminate\Http\Request;
use App\UserApp;

class NotifikasiController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $ustad = UserApp::SELECT('name')->WHERE ('user_category','=','1')->get();
		return view('admin.notifikasi.index', compact('ustad'));
	}

	public function sendnotif(){


    }
    function sendMessage($pesan){
        $content = array(
            "en" => $pesan
        );

        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'data' => array("foo" => "bar"),
            'contents' => $content
        );

        $fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }
}