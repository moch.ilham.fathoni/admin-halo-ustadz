<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
//use http\Env\Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class OrderController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $store = Order::WHERE('status', '=', '1')->orderBy('nama', 'asc')->get();
		return view('admin.store.index',compact('store'));
	}

	public function sudahtf(Request $request){
            $get = Order::find($request->id);
            $get->status = $request->status;
            $get->save();
            $data=[
              "message"=>"succes"
            ];
            return response()->json($data, 200);
    }
    public function destroy(Order $order, Request $request){
        $id = $request->id;

        Order::destroy($id);


        return redirect()->route('admin.order.index');
    }
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Order::destroy($toDelete);
        } else {
            Order::whereNotNull('id')->delete();

        }
        return redirect()->action('Admin\OrderController@index');

    }

}