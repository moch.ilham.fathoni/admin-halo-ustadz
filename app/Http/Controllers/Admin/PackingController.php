<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
//use http\Env\Request;
use Illuminate\Http\Request;

class PackingController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
    public function index()
    {
        $packing = Order::WHERE('status', '=', '2')->orderBy('nama', 'asc')->get();
        return view('admin.packing.index',compact('packing'));
    }

    public function alamat(Request $request){
        $get = Order::find($request->id);
        $get->alamat_lengkap = $request->alamat;
        $get->save();
        $data=[
            "message"=>"succes"
        ];
        return response()->json($data, 200);
    }
    public function detail(Request $request){
        $detail = Order::WHERE('id','=', $request->id)->get();
        $data=[
            "message"=>"succes",
            "result"=>$detail
        ];
        return response()->json($data, 200);
    }


}