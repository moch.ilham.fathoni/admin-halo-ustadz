<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\popupcloser;
use App\Http\Controllers\Traits\ImageUpload;
use Illuminate\Http\Request;

class PopupcloserController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
    public function index(popupcloser $popupcloser)
    {
        $popupcloser = $popupcloser->getcontent();
        return view('admin.popupcloser.index',compact('popupcloser'));

    }

    public function edit(popupcloser $popupcloser, $id){
        $popupcloser = $popupcloser->editcontent($id);
        return view('admin.popupcloser.edit',compact('popupcloser'));

    }
    public function update(Request $request, ImageUpload $imageUpload, popupcloser $popupcloser, $id){
        $type = 'popupcloser';
        $request = $imageUpload->saveImage($request, $type);
        $popupcloser->updatecontent($id, $request);
        return redirect('admin/popupcloser');
    }

}