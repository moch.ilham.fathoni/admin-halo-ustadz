<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ProfileUstad;
use Redirect;
use Schema;
use App\UserApp;
use App\Schedule;
use App\Http\Requests\CreateUstadRequest;
use App\Http\Requests\UpdateUstadRequest;
use Illuminate\Http\Request;

class ProfileUstadController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
		return view('admin.profileustad.index');
	}

    public function create()
    {
//        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);


        return view('admin.profilustad.create', compact("category"));
    }

    /**
     * Store a newly created artikel in storage.
     *
     * @param CreateArtikelRequest|Request $request
     */
    public function store(CreateArtikelRequest $request)
    {
        $request = $this->saveFiles($request);
        Artikel::create($request->all());

        return redirect()->route('content.artikel.index');
    }

    /**
     * Show the form for editing the specified artikel.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $artikel = Artikel::find($id);
        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);


        return view('admin.artikel.edit', compact('artikel', "category"));
    }

    /**
     * Update the specified artikel in storage.
     * @param UpdateArtikelRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateArtikelRequest $request)
    {
        $artikel = Artikel::findOrFail($id);

        $request = $this->saveFiles($request);

        $artikel->update($request->all());

        return redirect()->route('content.artikel.index');
    }

    /**
     * Remove the specified artikel from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Artikel::destroy($id);

        return redirect()->route('content.artikel.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Artikel::destroy($toDelete);
        } else {
            Artikel::whereNotNull('id')->delete();
        }

        return redirect()->route('content.artikel.index');
    }

}
