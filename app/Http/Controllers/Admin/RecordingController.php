<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\History_telepon;
use App\Http\Requests\CreateRecordingRequest;
use App\Http\Requests\UpdateRecordingRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\UserApp;
use App\Recording;
use Illuminate\Support\Facades\URL;
use App\Category;
use App\Model\App\Schedule_Content;
use DateTime;



class RecordingController extends Controller {

	/**
	 * Display a listing of recording
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
		$recording = Recording::orderByRaw('created_at DESC')->get();

		return view('admin.recording.index', compact('recording'));
//        return $jamaah;


	}

	private function getHistory(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/GetCallHistory/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&with_records=true",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Content-Type: application/json',
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);


        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            return $response;

        }
    }

	/**
	 * Show the form for creating a new recording
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $ustad = UserApp::SELECT("name", "id")->where('user_category','=','1')->get();
        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);



        return view('admin.recording.create', compact("ustad","category"));
	}

	/**
	 * Store a newly created recording in storage.
	 *
     * @param CreateRecordingRequest|Request $request
	 */
	public function store(Request $request, Schedule_Content $content)
	{
	    // $request = $this->saveFiles($request);
		// Recording::create($request->all());

		$request = $this->saveFilesAudio($request);
        $record = str_replace(' ','_',$request->recording_url);
        $url = URL::to("/public/uploads/recording");
        $recording = $url.'/'.$record;
        $post = new Recording();
        $post->id_ustad = $request->user_app_id;
        $post->id_category = $request->id_category;
        $post->judul = $request->name;
        $post->recording_url = $recording;
        $post->pertanyaan = $request->pertanyaan;
        $post->status = $request->status;
        $post->save();
        $last = $post->id;
        $image = "https://cp.haloustadz.id/public/images/rekaman_default.jpeg";
        $url = "https://cp.haloustadz.id/rekaman/".$last;
        $status = $request->status;
        $tanggal = $request->date_publish;
        $tgl_publish = date_format(new DateTime($tanggal), 'Y-m-d H:i:s');
        $type = "rekaman";

        $datacontent = ['post_categori'=>$type,'id_post'=>$last,'published'=>$tgl_publish,'status'=>$status];

        if($status == 1){
            $response_cotent = $content->adddata($datacontent);
        }
        elseif($status == 0){
            $this->sendMessage($request->name,$image,$url);
        }


		return redirect()->route(config('quickadmin.route').'.recording.index');
	}

	/**
	 * Show the form for editing the specified recording.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$recording = Recording::find($id);
	    $ustad = UserApp::pluck("nama_ustad", "id")->prepend('Please select', null);

	    
		return view('admin.recording.edit', compact('recording', "ustad"));
	}

	/**
	 * Update the specified recording in storage.
     * @param UpdateRecordingRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateRecordingRequest $request)
	{
		$recording = Recording::findOrFail($id);

        $request = $this->saveFiles($request);

		$recording->update($request->all());

		return redirect()->route(config('quickadmin.route').'.recording.index');
	}

	/**
	 * Remove the specified recording from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id, Schedule_Content $schedule)
	{
		Recording::destroy($id);
		$schedule->deletedata($id,'rekaman');


		return redirect()->route(config('quickadmin.route').'.recording.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Recording::destroy($toDelete);
        } else {
            Recording::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.recording.index');
    }
    function sendMessage($pesan, $image, $url){
        $content = array(
            "en" => $pesan

        );
        $fields = array(
            'app_id' => "7e742e94-fdc8-452b-98a7-e5f8b14eb6b4",
            'included_segments' => array('jamaah'),
            'excluded_segments' => array('notification_ustadz_online_unreceived'),
//            'include_player_ids' => ['a0985aaa-e66b-453d-a015-6d116877841a'],
            'data' => array("haloOfTheDayUrlImage" => $image, "haloOfTheDayUrlDetail" => $url, "haloOfTheDayTitle"=>$pesan, "type"=>"haloOfTheDay"),
            'contents' => $content,
            'small_icon' => 'img_halo_logo'
        );

        $fields = json_encode($fields);
//        print("\nJSON sent:\n");
//        print($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NjVjZmM5ODItYTljMS00MWVkLTg1YzgtODdiZjA5MjFiNWE0'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_exec($ch);
        curl_close($ch);


    }


}
