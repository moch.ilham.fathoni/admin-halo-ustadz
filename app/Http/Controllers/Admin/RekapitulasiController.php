<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\UserApp;
use App\Artikel;
use App\History_telepon;
use App\UstadOnline;
use Redirect;
use Schema;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RekapitulasiController extends Controller {


	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $id_ustad = 6332;
        $starttime = date("Y-m-d ");
        $endtime = date("Y-m-d");
        $ustad = UserApp::SELECT('id','name')->WHERE('user_category','1')->get();
        $data = array($starttime,$endtime,$ustad[0]);
        $report_data = UstadOnline::select(DB::raw('SEC_TO_TIME(sum(TIME_TO_SEC(history_ustad.total_time))) AS total_time, count(history_ustad.total_time) AS jumlahonline, DATE(history_ustad.start_time) AS date'))
            ->whereBetween('history_ustad.start_time', [$starttime, $endtime])
            ->WHERE('history_ustad.user_app_id','=',$id_ustad)
            ->groupBy(DB::raw("DATE(history_ustad.start_time)"))
            ->get();
        return view('admin.rekapitulasi.index',compact('report_data','ustad', 'data'));
	}
	public function getdata(Request $request){
        $id_ustad = $request->ustad;
        $starttime = $request->starttime;
        $endtime = $request->endtime;
        $ustad = UserApp::SELECT('id','name')->WHERE('user_category','1')->get();
        $data = array($starttime,$endtime,$id_ustad);
        $report_data = UstadOnline::select(DB::raw('SEC_TO_TIME(sum(TIME_TO_SEC(history_ustad.total_time))) AS total_time, count(history_ustad.total_time) AS jumlahonline, DATE(history_ustad.start_time) AS date'))
            ->whereBetween('history_ustad.start_time', [$starttime, $endtime])
            ->WHERE('history_ustad.user_app_id','=',$id_ustad)
            ->groupBy(DB::raw("DATE(history_ustad.start_time)"))
            ->get();
        return view('admin.rekapitulasi.index',compact('report_data','ustad', 'data'));
    }

}