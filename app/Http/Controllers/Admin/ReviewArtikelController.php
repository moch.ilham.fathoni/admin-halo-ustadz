<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\artikel;

class ReviewArtikelController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $artikel = Artikel::with("category")->get();
		return view('admin.reviewartikel.index');
	}



}