<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;

class RiwayatOrderController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
    public function index()
    {
        $r_order = Order::WHERE('status', '=', '5')->get();
        return view('admin.riwayatorder.index',compact('r_order'));
    }

}