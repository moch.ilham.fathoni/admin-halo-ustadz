<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\StokBuku;
use App\Order;
use Illuminate\Support\Facades\DB;


class StokBukuController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(StokBuku $stokbuku)
    {
        $sisa = Order::select(DB::raw('SUM(tafsir) as tafsir, SUM(tauhid) as tauhid,  SUM(bekal_haji) as bekal_haji '))->where('kategori','<=','1')->get();

        $stokbuku = $stokbuku->getdatabuku();
		return view('admin.stokbuku.index', compact('stokbuku','sisa'));
	}

}