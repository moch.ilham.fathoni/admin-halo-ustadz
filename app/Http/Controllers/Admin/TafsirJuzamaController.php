<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Tafsir_jusama;
//use http\Env\Request;
use Illuminate\Http\Request;
class TafsirJuzamaController extends Controller {

	/**
	 * Index page
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index()
    {
        $jusama = Tafsir_jusama::all();
		return view('admin.tafsirjuzama.index',compact('jusama'));
	}


    /**
     * Display a listing of category
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */


    /**
     * Show the form for creating a new category
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('admin.tafsirjuzama.create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param CreateCategoryRequest|Request $request
     */
    public function store(Request $request)
    {

        Tafsir_jusama::create($request->all());

        return view('admin.tafsirjuzama.create');
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $category = Tafsir_jusama::find($id);


        return view('admin.tafsirjuzama.edit', compact('category'));
    }

    /**
     * Update the specified category in storage.
     * @param UpdateCategoryRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, Request $request)
    {
        $category = Tafsir_jusama::findOrFail($id);



        $category->update($request->all());

        return redirect()->route(config('quickadmin.route').'.category.index');
    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Tafsir_jusama::destroy($id);

        return redirect()->route(config('quickadmin.route').'.tafsirjuzama.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Tafsir_jusama::destroy($toDelete);
        } else {
            Tafsir_jusama::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.tafsirjuzama.index');
    }

}