<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\UserApp;
use App\User;
use App\Schedule;
use App\Http\Requests\CreateUstadRequest;
use App\Http\Requests\UpdateUstadRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Category;
use App\Role_Category;
use App\ProfileUstad;


class UstadController extends Controller {


    /**
     * Display a listing of ustad
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $category = role_category::select('role_category.id', 'nama_category', 'user_app_id')->join('category', 'category.id', '=', 'role_category.category_id')
            ->join('user_app', 'role_category.user_app_id', '=', 'user_app.id')
            ->get();

        $schedule = Schedule::all();
        $categoryall = Category::pluck("nama_category", "id")->prepend('Please select', null);
        $ustad = UserApp::where('user_category' ,'=', '1')->with("category")->get();


        return view('admin.ustad.index', compact('ustad','schedule','category', 'categoryall'));

    }

    /**
     * Show the form for creating a new ustad
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);


        return view('admin.ustad.create', compact("category"));
    }

    /**
     * Store a newly created ustad in storage.
     *
     * @param CreateUstadRequest|Request $request
     */
    public function store(CreateUstadRequest $request)
    {
//        $request = $this->saveFiles($request);
        $password = $request->password_ustad;
        $nodeGeneratedHash=bcrypt($password);
        $finalNodeGeneratedHash = str_replace("$2y$", "$2a$", $nodeGeneratedHash);
        $username = $request->username;
        $name =  $request->name;
        $emailustad = $request->email;
        $asal = $request->asal;
        $riwayat_pend = $request->pendidikan;

//        $this->saveFiles($request);

        $a = $this->InsertFox($username, $password);
//        $this->InsertUserWeb($emailustad, $username, $nodeGeneratedHash);
        $myJSON = json_decode($a);
//        echo $myJSON->user_id;
        $post = new UserApp();
        $post->email = $request->email;
        $post->name     =  $name;
        $post->username = $username;
        $post->vox_id    = $myJSON->user_id;
        $post->category_id =  $request->category_id;
        $post->password     = $finalNodeGeneratedHash;
//        $foto = ''.$request->foto;
//        $post->foto    = $foto;
        $post->user_category     = 1;

        $post->save();
        $last = $post->id;
//        $iduser=UserApp::select('id')->where('email','=',$username)->get();
//        foreach ($iduser as $user_id){
//            $idus=$user_id->id;
//
//
//        }
        $this->InsertProfile( $last, $name, $asal, $riwayat_pend);
//		UserApp::create($request->all());

        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

    public function InsertFox($username, $password){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/AddUser/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&user_name=".$username."&user_display_name=".$username."&user_password=".$password."&application_id=5034824",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",

        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {


            return $response;

        }
    }



    public function InsertProfile($idus, $name, $asal, $riwayatpend){
        $post = new ProfileUstad();
        $post->user_app_id = $idus;
        $post->nama = $name;
        $post->domisili = $asal;
        $post->riwayat_pend = $riwayatpend;
        $post->save();
    }

    public function DelFox($userid){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/DelUser/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&user_id=$userid",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",

        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {


            return $response;

        }
    }

    public function ifActive($userid, $status){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.voximplant.com/platform_api/SetUserInfo/?account_id=2409891&api_key=75750bd4-3d52-46cc-850f-0b079207817f&user_id=$userid&user_active=$status",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",

        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            return "cURL Error #:" . $err;
        } else {


            return $response;

        }
    }


    /**
     * Show the form for editing the specified ustad.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ustad = UserApp::select('user_app.id','email','username','name','domisili','karya_tulis','profil_ustad.tempat_tgl_lahir','guru','profil_ustad.bidang_keilmuan','profil_ustad.riwayat_pend', 'password')->join('profil_ustad','profil_ustad.user_app_id','=','user_app.id')->find($id);
//       return $ustad;
//        $category = Category::pluck("nama_category", "id")->prepend('Please select', null);
        return view('admin.ustad.edit', compact('ustad'));
    }

    /**
     * Update the specified ustad in storage.
     * @param UpdateUstadRequest|Request $request
     *
     * @param  int  $id
     */
    public function update($id, UpdateUstadRequest $request)
    {
//        $ustad = UserApp::findOrFail($id);
//        $request = $this->saveFiles($request);
//        $ustad->update($request->all());

        $password = $request->password_ustad;
        $nodeGeneratedHash=bcrypt($password);
        $finalNodeGeneratedHash = str_replace("$2y$", "$2a$", $nodeGeneratedHash);
        $name =  $request->name;


        if(empty($password)){
            $post = UserApp::find($id);
            $post->email = $request->id_ustad;
            $post->name     =  $name;
            $post->category_id =  $request->category_id;
            $post->save();

        }
        else{
            $post = UserApp::find($id);
            $post->email = $request->id_ustad;
            $post->name     =  $name;
            $post->category_id =  $request->category_id;
            $post->password     = $finalNodeGeneratedHash;
            $post->save();
        }


        ProfileUstad::WHERE('user_app_id','=', $id)
            ->update([
                'nama' => $request->name,
                'domisili' => $request->domisili,
                'riwayat_pend' => $request->riwayat_pend,
                'karya_tulis' => $request->karya_tulis,
                'tempat_tgl_lahir' => $request->tempat_tgl_lahir,
                'guru' => $request->guru,
                'bidang_keilmuan' => $request->bidang_keilmuan
            ]);
//        $post->nama = $request->name;
//        $post->asal = $request->asal;;
//        $post->riwayat_pend = $request->pendidikan;
//        $post->update();
        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

    /**
     * Remove the specified ustad from storage.
     *
     * @param  int  $id
     */
    public  function  deletedata($id){
        Role_Category::destroy($id);

    }

    public function destroy($id)
    {
        $ustad = UserApp::select('vox_id','id ')->where('user_category' ,'=', '1')->where('id','=',$id)->get();
        foreach ($ustad as $row){
            $id_voxim = $row->vox_id;
            $this->DelFox($id_voxim);
            $this->deletedata($id);
        }
        UserApp::destroy($id);
        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            UserApp::destroy($toDelete);
        } else {
            UserApp::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

    public function isactive($id, $is_active){


    }

    public function blackList(Request $request){
        $is_active = $request->is_active;
        $id = $request->id;
//        $this->isactive($id, $is_active);
        $id_voxim=UserApp::select('vox_id')->where('id','=',$id)->get();
        foreach ($id_voxim as $vox_id){
            $voxid=$vox_id->vox_id;
        }
        if($is_active == 1 ){
            $status = "false";
        }
        else{
            $status = "true";
        }
//
        $post = UserApp::find($id);
        $post->is_active = $is_active;

        $post->save();
//        $last = dd($post->vox_id);
        $a = $this->ifActive($voxid, $status);
        return redirect()->route(config('quickadmin.route').'.ustad.index');

//        echo $status;


    }


    public function DestroyCategory($id){
//        $$id = $request->;
//        UserApp::destroy($id);
        return $id;
//        return redirect()->route(config('quickadmin.route').'.ustad.index');
    }

}
