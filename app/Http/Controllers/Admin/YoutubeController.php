<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Youtube;
use Illuminate\Http\Request;


class YoutubeController extends Controller {

    /**
     * Index page
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $youtube_list = Youtube::WHERE('is_live','=','0')->orderByRaw('created_at DESC')->get();
        $youtube_live = Youtube::WHERE('is_live','=','1')->orderByRaw('created_at DESC')->get();

        return view('admin.youtube.index',compact('youtube_list','youtube_live'));
    }

    public function update($id, Request $request)
    {
        $youtube = Youtube::findOrFail($id);



        $youtube->update($request->all());

        return redirect()->route('admin.youtube.index');
    }

    public function create()
    {


        return view('admin.youtube.create');
    }

    /**
     * Store a newly created category in storage.
     *
     * @param CreateCategoryRequest|Request $request
     */
    public function store(Request $request)
    {

//        Youtube::create($request->all());
        $post = new Youtube();
        $post->title = $request->title;
        $post->url   = $request->url;
        $post->is_live   = 0;
        $post->status   = 0;
        $post->save();
//        return $request->all();
        return redirect()->route('admin.youtube.index');
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $youtube = Youtube::find($id);
        if($id == 1){
            $this->emityoutube();
        }


        return view('admin.youtube.edit', compact('youtube'));
    }

    /**
     * Update the specified category in storage.
     * @param UpdateCategoryRequest|Request $request
     *
     * @param  int  $id
     */


    /**
     * Remove the specified category from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Youtube::destroy($id);

        return redirect()->route('admin.youtube.index');
    }

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Category::destroy($toDelete);
        } else {
            Category::whereNotNull('id')->delete();
        }

        return redirect()->route('admin.youtube.index');
    }
    function emityoutube(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "3000",
            CURLOPT_URL => "http://localhost:3000/emitdata",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "",
            CURLOPT_HTTPHEADER => array(
                "Postman-Token: 179f867f-1efa-457d-829d-50a731719dbb",
                "cache-control: no-cache"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }




}