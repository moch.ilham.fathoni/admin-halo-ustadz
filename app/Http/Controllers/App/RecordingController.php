<?php

namespace App\Http\Controllers\App;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Recording;

class RecordingController extends Controller
{
    //
    public function index(Recording $recording, $id)
    {
        $data_recording = $recording->getdatarecord($id);

//        return $data_recording;
        return view('app.recording.recording',compact('data_recording'));

    }
    public function recording(Recording $recording, $id)
    {
        $data_recording = $recording->getdatarecord($id);

//        return $data_recording;
        return view('app.recording.rekaman',compact('data_recording'));

    }
    public function player2(Recording $recording, $id)
    {
        $data_recording = $recording->getdatarecord($id);

//        return $data_recording;
        return view('app.recording.player2',compact('data_recording'));

    }
}
