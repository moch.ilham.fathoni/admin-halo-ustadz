<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

class Artikel extends Model {

    protected $table    = 'artikel';

    protected $fillable = [
        'id_ustad',
        'category_id',
        'judul',
        'isi_artikel',
        'image',
        'status'
    ];


    public static function boot()
    {
        parent::boot();

        // Artikel::observe(new UserActionsObserver);
    }

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }





}