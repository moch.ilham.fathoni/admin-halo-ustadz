<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Schema;


class Doa extends Model
{
    //
    protected $table    = 'doa_harian';

    protected $fillable = [
        'id',
        'title',
        'image',
        'content_arab',
        'content_text',
        'status',
        'created_at'
    ];
    function getdatadoa(){
        $doa = Doa::orderByRaw('created_at DESC')->paginate(10);
        return $doa;
    }
    function addnew(Request $request){
        $doa = Doa::create($request->all());
        $doa = $doa->id;
        return $doa;
    }
    function deletecontent($id){
        $doa = Doa::destroy($id);
        return $doa;
    }
    function editcontent($id){
        $doa = Doa::find($id);
        return $doa;
    }
    function updatecontent($id, Request $request){
        $doa = Doa::findOrFail($id);
        $doa->update($request->all());
        return $doa;
    }
}
