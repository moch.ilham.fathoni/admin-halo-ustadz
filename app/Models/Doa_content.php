<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doa_content extends Model
{
    //
    protected $table    = 'doa_isi';

    protected $fillable = [
        'name',
        'subtitle',
        'caption',
        'text_arab',
        'text_latin',
        'terjemah',
        'title_id'

    ];
}
