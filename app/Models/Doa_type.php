<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doa_type extends Model
{
    //

    protected $table    = 'doa_type';

    protected $fillable = [
        'name'

    ];
}
