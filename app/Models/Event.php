<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

class Event extends Model {
    protected $table    = 'event';
    protected $fillable = [
        'category_id',
        'nama',
        'image',
        'deskripsi',
        'category_post'
    ];
    

    public static function boot()
    {
        parent::boot();

        // Event::observe(new UserActionsObserver);
    }
    
    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }

}