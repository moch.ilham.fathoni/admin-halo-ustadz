<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Schema;


class Hadits extends Model
{
    //
    protected $table    = 'hadits_harian';

    protected $fillable = [
        'id',
        'title',
        'image',
        'content_arab',
        'content_text',
        'content_riwayat',
        'no_hadits',
        'status',
        'created_at'
    ];

    function getdatahadits(){
        $hadits = Hadits::orderByRaw('created_at DESC')->paginate(10);
        return $hadits;
    }
    function addnew(Request $request){
        $hadits = Hadits::create($request->all());
        $hadits = $hadits->id;
        return $hadits;
    }
    function deletecontent($id){
        $hadits = Hadits::destroy($id);
        return $hadits;
    }
    function editcontent($id){
        $hadits = Hadits::find($id);
        return $hadits;
    }
    function updatecontent($id, Request $request){
        $hadits = Hadits::findOrFail($id);
        $hadits->update($request->all());
        return $hadits;
    }

}
