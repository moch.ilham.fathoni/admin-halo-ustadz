<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Laraveldaily\Quickadmin\Observers\UserActionsObserver;




class History_telepon extends Model {

    protected $table    = 'history_telepon';
    
    protected $fillable = [
          'start_time',
          'end_time',
          'record_url',
          'duration',
          'history_ustad_id',
        'jamaah_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        // History_telepon::observe(new UserActionsObserver);
    }
    
//    public function ustad()
//    {
//        return $this->hasOne('App\UserApp', 'id', 'ustad_id');
//    }



    
    
    
}