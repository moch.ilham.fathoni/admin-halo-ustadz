<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    protected $table    = 'buku_tafsir';

    protected $fillable = [
        'nama',
        'tlpn',
        'jumlah_buku',
        'provinsi',
        'kota',
        'alamat_lengkap',
        'harga_ongkir',
        'total_harga',
        'status',
        'resi'
    ];
}
