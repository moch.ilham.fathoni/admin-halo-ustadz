<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileUstad extends Model
{
    //
    protected $table    = 'profil_ustad';

    protected $fillable = [
        'nama',
        'domisili',
        'riwayat_pend',
        'karya_tulis',
        'tempat_tgl_lahir',
        'guru',
        'bidang_keilmuan',
        'user_app_id'

    ];
}
