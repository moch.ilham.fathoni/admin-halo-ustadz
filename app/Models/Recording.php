<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recording extends Model
{
    //
    protected $table    = 'recording';

    protected $fillable = [
        'id',
        'judul',
        'image',
        'pertanyaan',
        'recording_url',
        'id_ustad',
        'id_category',
        'status',
        'created_at'
    ];

    function getdatarecord($id){
        $recording = Recording::
        SELECT('recording.id','judul','image','pertanyaan','recording_url','user_app.name AS ustad','category.nama_category AS category', 'recording.created_at')
            ->JOIN('user_app','recording.id_ustad','user_app.id')
            ->JOIN('category','recording.id_category','category.id')
            ->WHERE('recording.id','=',$id)->get();
        return $recording;
    }
}
