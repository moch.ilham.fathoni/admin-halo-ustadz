<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class RoleCategory extends Model
{
    protected $table    = 'role_category';

    protected $fillable = [
        'category_id',
        'user_app_id'
    ];
}
