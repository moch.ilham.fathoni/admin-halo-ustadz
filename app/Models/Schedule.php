<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
// use Laraveldaily\Quickadmin\Observers\UserActionsObserver;

class Schedule extends Model {

    

    

    protected $table    = 'schedule';
    
    protected $fillable = [
          'user_id',
          'hari',
          'start_time',
          'end_time',
            'user_app_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        // Schedule::observe(new UserActionsObserver);
    }


    
    
    
}