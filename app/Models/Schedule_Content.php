<?php

namespace App\Models;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Schedule_Content extends Model
{
    //
    protected $table    = 'post_schadule';

    protected $fillable = [
        'id',
        'post_categori',
        'id_post',
        'published',
        'status',
        'created_at',
        'updated_at'
    ];


    function getdata(){
        $hadits = Schedule_Content::orderByRaw('created_at DESC')->get();
        return $hadits;
    }
    function adddata($request){
        $content = Schedule_Content::insert($request);
        return $content;
    }
    function deletedata($id,$category){
        $hadits = Schedule_Content::where('id_post','=',$id)
            ->where('post_categori','=',$category)
            ->delete();
        return $hadits;
    }
    function editdata($id){
        $hadits = Schedule_Content::find($id);
        return $hadits;
    }
    function updateupdate($id, Request $request){
        $hadits = Schedule_Content::findOrFail($id);
        $hadits->update($request->all());
        return $hadits;
    }

}

