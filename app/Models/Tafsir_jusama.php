<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tafsir_jusama extends Model
{
    //
    protected $table    = 'tafsir_content';

    protected $fillable = [
        'page_name',
        'title',
        'content',
        'tafsir_surah_id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
