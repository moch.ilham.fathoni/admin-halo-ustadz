<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserApp extends Model
{
    use HasFactory;
    protected $dates = ['deleted_at'];

    protected $table    = 'user_app';
    
    protected $fillable = [
          'email',
          'name',
        'username',
        'vox_id',
          'category_id',
          'password',
            'user_category',
            'foto',
        'tlp',
        'status'

    ];
    

    public static function boot()
    {
        parent::boot();

        // UserApp::observe(new UserActionsObserver);
    }
    
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }




    /**
     * Hash password
     * @param $input
     */
    public function setPasswordUstadAttribute($input)
    {
        $this->attributes['password_ustad'] = Hash::make($input);
    }
}
