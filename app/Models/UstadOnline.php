<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UstadOnline extends Model
{
    //
    protected $table    = 'history_ustad';

    protected $fillable = [
        'id',
        'start_time',
        'end_time',
        'user_app_id',
        'total_time'

    ];

    public static function boot()
    {
        parent::boot();
        UstadOnline::observe(new UstadOnline);
    }


}
