<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    //
    protected $dates = ['deleted_at'];

    protected $table    = 'youtube';

    protected $fillable = [
        'id',
        'title',
        'url',
        'is_live',
        'status'

    ];
}
