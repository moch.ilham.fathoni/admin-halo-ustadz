@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}">
    <script type="text/javascript" src="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
@endsection

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>Add New Artikel</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(array('files' => true, 'route' => 'content.artikel.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                <div class="form-group">
                    {!! Form::label('id_ustad', 'Nama Ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">

                        <select name="id_ustad" id="id_ustad" class="form-control form-control-primary">
                            @foreach($ustad as $row1)
                                <option value="{{ $row1->id }}">{{ $row1->name }}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('category_id', 'Category*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::select('category_id', $category, old('category_id'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('judul', 'Judul*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('judul', old('judul'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('isi_artikel', 'Isi Artikel*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('isi_artikel', old('isi_artikel'), array('class'=>'form-control my-editor')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('image', 'image*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::file('image') !!}
                        {!! Form::hidden('image_w', 4096) !!}
                        {!! Form::hidden('image_h', 4096) !!}

                    </div>
                </div>
                <div class="form-group" id="hidden_post" style="display: none;">
                    {!! Form::label('Tanggal Publish', 'Tanggal Publish*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        <input class="form-control form_datetime" type="text" name="date_publish" readonly>
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('status', 'Status*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        <select id="status" onchange="changestatus()" name="status" class="form-control">
                            <option value="0">Posting Sekarang</option>
                            <option value="1">Jadwalkan Postingan</option>
                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('Save') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        function changestatus() {
            status = $("#status").val();
            if(status == 1){
                $("#hidden_post").css('display','block');
            }
            else if(status == 0){
                $("#hidden_post").css('display','none');
            }
        }
    </script>
@endsection
