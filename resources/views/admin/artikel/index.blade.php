@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="ablepro/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="ablepro/assets/pages/data-table/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="ablepro/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css">
    <!-- Style.css -->
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5> </h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                {!! link_to_route('content.artikel.create', trans('Add New') , null, array('class' => 'btn btn-success')) !!}
                </div>
                @if ($artikel->count())
                    <div class="table-responsive dt-responsive">
                        <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                            <thead>
                            <tr>
                                <th>
                                    {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                                </th>
                                <th>Nama Ustad</th>
                                <th>Category</th>
                                <th>Judul</th>
                                <th>image</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($artikel as $row)
                                <tr>
                                    <td>
                                        {!! Form::checkbox('del-'.$row->id_artikel,1,false,['class' => 'single','data-id'=> $row->id_artikel]) !!}
                                    </td>
                                    <td>{{ $row->name }}</td>
                                    <td>{{ $row->nama_category }}</td>
                                    <td>{{ $row->judul }}</td>
                                    <td><img width="100px" src="{{ $row->image }}"></td>
                                    <td>


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-sm btn-danger" id="delete">
                                    Delete
                                </button>
                            </div>
                        </div>
                         <!-- Form::open(['route' => config('quickadmin.route').'.artikel.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!} -->
                        <input type="hidden" id="send" name="toDelete">
                         <!-- Form::close() !!} -->
                    </div>
            </div>
            @else
                {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
            @endif
        </div>
    </div>
    </div>
    </div>
@endsection
@section('javascript')
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });
    </script>
@stop
