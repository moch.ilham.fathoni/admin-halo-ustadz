@extends('admin.layouts.master')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/pages/notification/notification.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/css/pages.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/css/style.css') }}">
    <style>
        .loader-block{
            top: 50%;
            position: fixed;
            left: 45%;
        }
    </style>
@endsection
@section('content')
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">

                            <h4 class="text-c-yellow">{{  $userapp }}</h4>
                            <h6 class="text-muted m-b-0">Total Jumlah Ustad</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="feather icon-bar-chart-2 f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-yellow">
                    <div class="row align-items-center">
                        <div class="col-9">
                            <p class="text-white m-b-0">% change</p>
                        </div>
                        <sdiv class="col-3 text-right">
                            <i class="feather icon-trending-up text-white f-16"></i>
                        </sdiv>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h4 class="text-c-green">{{ $jamaah  }}</h4>
                            <h6 class="text-muted m-b-0">Total Jumlah Jamaah</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="feather icon-file-text f-28"></i>
                        </div>

                    </div>
                </div>
                <div class="card-footer bg-c-green">
                    <div class="row align-items-center">
                        <div class="col-9">
                            <p class="text-white m-b-0">% change</p>
                        </div>
                        <div class="col-3 text-right">
                            <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="col-8">

                            <h4 class="text-c-green">290+</h4>
                            <h6 class="text-muted m-b-0">Log Panggilan</h6>
                        </div>
                        <div class="col-4 text-right">
                            <i class="feather icon-file-text f-28"></i>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-c-green">
                    <div class="row align-items-center">
                        <div class="col-9">
                            <p class="text-white m-b-0">% change</p>
                        </div>
                        <div class="col-3 text-right">
                            <i class="feather icon-trending-up text-white f-16"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-block">
                    <div class="row align-items-center">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <td> Nama</td>
                                    <td> Status</td>
                                    <td> Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ustad as $raw)
                                    <tr>
                                        <td>{{ $raw->name }}</td>
                                        <td>
                                            @if($raw->status === 1)
                                                <label id="label-{{ $raw->id }}" class="label bg-success">Online</label>
                                            @else
                                                <label class="label bg-secondary">Offline</label>
                                            @endif
                                        </td>
                                        <td>
                                                <button onclick="status({{$raw->id}})" class="btn btn-mini btn-success">Matikan</button>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>

                            </table>
                        </div>

                    </div>
                </div>
                <div class="card-footer bg-c-yellow">
                    <div class="row align-items-center">
                        <div class="col-9">
                            <p class="text-white m-b-0">Status Ustad</p>
                        </div>
                        <sdiv class="col-3 text-right">
                            <i class="feather icon-trending-up text-white f-16"></i>
                        </sdiv>
                    </div>
                </div>
            </div>
        </div>
        <div class="loader-block">
            <svg id="loader2" viewBox="0 0 100 100">
                <circle id="circle-loader2" cx="50" cy="50" r="45"></circle>
            </svg>
        </div>
        <div class="notifications">
        </div>
@endsection
@section('js')
    <script type="text/javascript" src="{{ URL::asset('ablepro/assets/js/script.js') }}"></script>
    <script src="{{ URL::asset('ablepro/assets/js/bootstrap-growl.min.js') }}"></script>

    <script>
        $( document ).ready(function() {
            $(".loader-block").hide();
        });
     function status(id) {
         $(".loader-block").show();
         var form = new FormData();
         form.append("id", id);
         var url = "{{ url('/') }}";
         var settings = {

             "async": true,
             "crossDomain": true,
             "url": url+"/api/status_ustad",
             "method": "POST",
             "headers": {
                 "id": "25632",
                 "status": "0",
                 "Access-Control-Allow-Origin": "*",
                 "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFuYXNidXJoYW51ZGRpbkBoYWxvdXN0YWR6LmlkIiwidXNlcm5hbWUiOiJhbmFzYnVyaGFudWRkaW4iLCJpYXQiOjE1NDc0NzI0NjZ9.bAmT2CEYFusuMRQrcnbKyZpF1YltdulmxY8iAanygzA",
                 "cache-control": "no-cache",
                 "Postman-Token": "87df5372-ac81-40c6-9a11-96e7d9ce7d02"
             },
             "processData": false,
             "contentType": false,
             "mimeType": "multipart/form-data",
             "data": form
         }

         $.ajax(settings).done(function (response) {
             if(response === 'success'){
                 notify('Sukses ','Status Telah di rubah');
                 $(".loader-block").hide();
                 document.getElementById("label-"+id).className = "label bg-secondary";

             }
         });
     }
     function notify(title, msg){
         var from = 'bottom';
         var align = 'right';
         var icon = '';
         var type = 'inverse';
         var animIn = '';
         var animOut = '';
         $.growl({
             icon: icon,
             title: title,
             message: msg,
             url: ''
         },{
             element: 'body',
             type: type,
             allow_dismiss: true,
             placement: {
                 from: from,
                 align: align
             },
             offset: {
                 x: 30,
                 y: 30
             },
             spacing: 10,
             z_index: 999999,
             delay: 2500,
             timer: 1000,
             url_target: '_blank',
             mouse_over: false,
             animate: {
                 enter: animIn,
                 exit: animOut
             },
             icon_type: 'class',
             template: '<div data-growl="container" class="alert" role="alert">' +
             '<button type="button" class="close" data-growl="dismiss">' +
             '<span aria-hidden="true">&times;</span>' +
             '<span class="sr-only">Close</span>' +
             '</button>' +
             '<span data-growl="icon"></span>' +
             '<span data-growl="title"></span>' +
             '<span data-growl="message"></span>' +
             '<a href="#" data-growl="url"></a>' +
             '</div>'
         });
     };

 </script>
@endsection