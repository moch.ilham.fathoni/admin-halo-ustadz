@extends('admin.layouts.master')

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(array('route' => 'doa.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}
                <div class="form-group">
                    {!! Form::label('title_id', ' title_id*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        <select class="form-control form-control-primary" name="title_id">
                            @foreach($Doa_type as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('subtitle', 'subtitle*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('subtitle', old('subtitle'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('caption', 'caption*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('caption', old('caption'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('text_arab', 'text_arab*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('text_arab', old('text_arab'), array('class'=>'form-control ')) !!}

                    </div>
                </div> <div class="form-group">
                    {!! Form::label('text_latin', 'text_latin*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('text_latin', old('text_latin'), array('class'=>'form-control ')) !!}

                    </div>
                </div> <div class="form-group">
                    {!! Form::label('terjemah', 'Terjemah*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('terjemah', old('terjemah'), array('class'=>'form-control ')) !!}

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection