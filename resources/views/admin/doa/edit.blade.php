@extends('admin.layouts.master')

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>
                {!! Form::model($Doa_content, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array('doa.update', $Doa_content->id))) !!}

                <div class="form-group">
                    {!! Form::label('title_id', ' title_id*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        <select class="form-control form-control-primary" name="title_id">
                            @foreach($Doa_type as $row)
                                @if($Doa_content->subtitle === $row->id)
                                    <option value="{{$row->id}}">{{$row->name}}</option>
                                    @endif
                                <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('subtitle', 'subtitle*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('subtitle', old('subtitle', $Doa_content->subtitle), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('caption', 'caption*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('caption', old('caption', $Doa_content->caption), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('text_arab', 'Isi text_arab*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('text_arab', old('text_arab', $Doa_content->text_arab), array('class'=>'form-control ')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('text_latin', 'text_latin*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('text_latin', old('text_latin', $Doa_content->text_latin), array('class'=>'form-control ')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('terjemah', 'terjemah*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('terjemah', old('terjemah', $Doa_content->terjemah), array('class'=>'form-control ')) !!}

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection