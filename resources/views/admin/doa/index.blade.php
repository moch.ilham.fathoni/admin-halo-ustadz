@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>Doa List</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <button class="btn btn-mini btn-primary" data-toggle="modal" data-target="#default-Modal">Add New Category Doa</button>
                </div>


                <div class="modal fade" id="default-Modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Jadawal Ustad</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            {!! Form::open(array('action' => 'App\Http\Controllers\Admin\DoaController@createcategorydoa', 'method' =>'POST', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                            <div class="modal-body">

                                {!! Form::label('Name', 'Name Category*', array('class'=>'control-label')) !!}
                                <div class="col-md-12">
                                    <div class="row">
                                            <input style="color: black;" type="text" name="name" class="form-control form-control-primary"" />

                                    </div>
                                </div>



                            </div>
                            <div class="modal-footer">

                                <div class="form-group">
                                    <div class="col-md-12 col-sm-offset-2">
                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

                                        <button class="btn btn-sm btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </div>
                            {!! Form::close() !!}

                        </div>
                    </div>
                </div>

                @if ($Doa_type->count())

                    <div class="row">
                        @foreach ($Doa_type as $row)
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">
                                        {{ $row->name }}
                                    </h5>
                                    <div id="nestable-menu" class="m-b-10">
                                        {!! link_to_action('App\Http\Controllers\Admin\DoaController@createdoa', 'Add Doa', array($row->id), array('class' => 'btn btn-mini btn-info')) !!}

                                    </div>
                                    <p class="card-text">
                                    <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                        <thead>
                                        <tr>
                                            <th>Subtitle</th>
                                            <th>Caption&nbsp;</th>
                                            <th></th>
                                        </tr>
                                        </thead>

                                        <tbody>
                                        @foreach ($Doa_content as $row1)
                                            @if($row->id == $row1->title_id)
                                        <tr>
                                            <td>
                                                {!! $row1->subtitle !!}
                                            </td>
                                            <td>
                                                {!! $row1->caption !!}
                                            </td>
                                            <td>
                                                {!! link_to_action('App\Http\Controllers\Admin\DoaController@delete', 'Delete', array($row1->id), array('class' => 'btn btn-mini btn-danger')) !!}
                                                {!! link_to_action('App\Http\Controllers\Admin\DoaController@edit', 'Edit', array($row1->id), array('class' => 'btn btn-mini btn-primary')) !!}
                                            </td>
                                        </tr>
                                            @endif
                                        @endforeach
                                        </tbody>
                                    </table>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
            </div>
            @else
                {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
            @endif
        </div>

    </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop