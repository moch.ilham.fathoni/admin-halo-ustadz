@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}">
    <script type="text/javascript" src="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>Doa Harian</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalScrollable">
                        Add New
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Add New</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(array('files' => true, 'action' => 'App\Http\Controllers\Admin\DoaHarianController@store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}
                                    <div class="form-group">
                                        {!! Form::label('judul', 'Judul*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::text('title', old('title'), array('class'=>'form-control form-control-primary')) !!}
                                        </div>
                                    </div><div class="form-group">
                                        {!! Form::label('Arab', 'Arab*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::textarea('content_arab', old('content_arab'), array('class'=>'form-control my-editor')) !!}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Text', 'Text*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::textarea('content_text', old('content_text'), array('class'=>'form-control my-editor')) !!}

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::label('image', 'image*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::file('image') !!}
                                            {!! Form::hidden('image_w', 4096) !!}
                                            {!! Form::hidden('image_h', 4096) !!}

                                        </div>
                                    </div>
                                    <div class="form-group" id="hidden_post" style="display: none;">
                                        {!! Form::label('Tanggal Publish', 'Tanggal Publish*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            <input class="form-control form_datetime" type="text" name="date_publish" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('status', 'Status*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            <select id="status" onchange="changestatus()" name="status" class="form-control">
                                                <option value="0">Posting Sekarang</option>
                                                <option value="1">Jadwalkan Postingan</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-offset-2">
                                            {!! Form::submit( trans('Simpan') , array('class' => 'btn btn-primary')) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Edit</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(array('files' => true, 'action' => 'App\Http\Controllers\Admin\HaditsController@store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}
                                    <div class="form-group">
                                        {!! Form::label('judul', 'Judul*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::text('title', old('title'), array('class'=>'form-control form-control-primary')) !!}
                                        </div>
                                    </div><div class="form-group">
                                        {!! Form::label('Arab', 'Arab*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::textarea('content_arab', old('content_arab'), array('class'=>'form-control my-editor')) !!}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Text', 'Text*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::textarea('content_text', old('content_text'), array('class'=>'form-control my-editor')) !!}

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('Riwayat', 'Riwayat*', array('class'=>'control-label')) !!}
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    {!! Form::text('content_riwayat', old('content_riwayat'), array('class'=>'form-control', 'placeholder'=>'Riwayat')) !!}
                                                </div>
                                                <div class="col-md-2">
                                                    {!! Form::text('no_hadits', old('no_hadits'), array('class'=>'form-control', 'placeholder'=>'No')) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('image', 'image*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::file('image') !!}
                                            {!! Form::hidden('image_w', 4096) !!}
                                            {!! Form::hidden('image_h', 4096) !!}

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-offset-2">
                                            {!! Form::submit( 'Simpan' , array('class' => 'btn btn-primary')) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                    @if ($data_doa->count())
                        <div class="table-responsive dt-responsive">
                            <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                                    </th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($data_doa as $row)
                                    <tr>
                                        <td>
                                            {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                                        </td>
                                        <td>{{ $row->title }}</td>
                                        <td><img class="img-responsive" width="200px" src="{{ $row->image }}"></td>
                                        <td>
                                            {{--{!! link_to_route(config('quickadmin.route').'.artikel.edit', trans('quickadmin::templates.templates-view_index-edit'), array($row->id), array('class' => 'btn btn-sm btn-info')) !!}--}}
                                            {{--<button onclick="editdata({{ $row->id }})" class="btn btn-sm btn-info" value="edit">Edit</button>--}}
                                            {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array('admin.doaharian.destroy',$row->id))) !!}
                                            {!! Form::submit(trans('Delete'), array('class' => 'btn btn-sm btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-xs-12">
                                    {{ $data_doa->links("pagination::bootstrap-4") }}
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-sm btn-danger" id="delete">
                                    delete checked
                                    </button>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'doaharian.artikel.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                            <input type="hidden" id="send" name="toDelete">
                            {!! Form::close() !!}
                        </div>
                </div>
                @else
                    {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
                @endif
            </div>

        </div>
    </div>
    </div>
    <script>
        $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        function changestatus() {
            status = $("#status").val();
            if(status == 1){
                $("#hidden_post").css('display','block');
            }
            else if(status == 0){
                $("#hidden_post").css('display','none');
            }
        }
    </script>
@endsection

@section('javascript')
    <!-- Custom js -->

    <script>

        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });


        function editdata(id) {
            getdata(id)

            $('#edit').modal('show');

        }
        function getdata(id) {
            var url={{ url('/') }}+"/api/hadits"+id;
            var settings = {
                "async": true,
                "crossDomain": true,
                "method": "GET",
                "headers": {
                    "cache-control": "no-cache",
                    "Postman-Token": "30d21590-8c00-4a63-b09a-daffa2bddac6",
                    "Access-Control-Allow-Origin": "*",
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Authorization, X-Request-With',
                },

            }
            $.getJSON(url,settings, function(response)
            {
                console.log(response);

            });

        }

    </script>

@stop
