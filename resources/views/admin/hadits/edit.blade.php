@extends('admin.layouts.master')

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::model($data_hadits, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'action' => array('App\Http\Controllers\Admin\HaditsController@update', $data_hadits->id))) !!}

                    <div class="form-group">
                        <label class="control-label">Judul*</label>
                        <div class="col-md-12">
                            <input name="title" value="{{ $data_hadits->title }}" class="form-control form-control-primary" id="title">
                        </div>
                    </div><div class="form-group">
                        <label class="control-label">Arab*</label>
                        <div class="col-md-12">
                            <textarea name="content_arab" class="form-control my-editor" id="content_arab">{{ $data_hadits->content_arab }}</textarea>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Text*</label>

                        <div class="col-md-12">
                            {{--                                            {!! Form::textarea('content_text', old('content_text'), array('class'=>'form-control my-editor', 'id'=>'content_text')) !!}--}}
                            <textarea name="content_text" class="form-control my-editor" id="content_text">{{ $data_hadits->content_text }}</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Riwayat*</label>

                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4">
                                    <input name="content_riwayat" value="{{ $data_hadits->content_riwayat }}" class="form-control" id="riwayat">
                                </div>
                                <div class="col-md-2">
                                    <input name="no_hadits" value="{{ $data_hadits->no_hadits }}" class="form-control" id="no_hadits">

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </div>
                    <div id="form_edit">

                    </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
