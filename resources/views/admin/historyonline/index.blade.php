@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/css/pages.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/css/style.css') }}">
    <style>
        .loader-block{
            top: 50%;
            position: fixed;
            left: 45%;
            z-index: 5;
        }
    </style>
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div class="col-md-12">

                    <div class="sub-title">
                        {{ Form::open(array('action' => 'Admin\HistoryOnlineController@getdata', 'method' => 'post')) }}
                        <input type="date" class="" name="starttime" placeholder="hdsafds"> - <input type="date" class="" name="endtime">
                        <select name="ustad">
                            @foreach ($ustad as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-mini btn-info" type="submit"><i class="fa fa-print"></i>Submit</button>
                        {{ Form::close() }}
                        {{ Form::open(array('action' => 'Admin\HistoryOnlineController@rekap', 'method' => 'post')) }}
                        <?php
                        $starttime = date("Y-m-d "."00:00:00");
                        $endtime = date("Y-m-d"." 23:59:00");
                        ?>
                        <input type="date" class="" value="{{ $starttime }}" name="starttime" hidden> - <input type="date" class="" value="{{ $endtime }}" name="endtime" hidden>
                        <input hidden value="81" name="ustad">
                        <button class="btn btn-mini btn-info float-right" type="submit">Cetak Semua Ustad</button>
                        {{ Form::close() }}
                    </div>
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <div class="tab-content card-block">
                        @include('admin.historyonline.table_ustad')

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ URL::asset('ablepro/assets/js/script.js') }}"></script>
    <script src="{{ URL::asset('ablepro/assets/js/bootstrap-growl.min.js') }}"></script>

    <script src="{{ URL::asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/js/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('/js/vfs_fonts.js') }}"></script>


    <script>
        // document.getElementById("loader").style.display('none');
        $("#loader").hide();

        function get_data(id) {
            document.getElementById("employee_table").innerHTML = '';
            $(".loader-block").show();
            console.log(1);
            var url = '{{ url('/') }}';

            var form = new FormData();
            form.append("history", id);
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url+"/api/get_rekaman",
                "method": "POST",
                "headers": {
                    "cache-control": "no-cache",
                    "Postman-Token": "e5506e9f-8939-4cdd-8e7e-c773e3b06ca0"
                },
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            }

            $.getJSON(settings, function(response){

                // console.log(response);
                var employee_data = '';
                $.each(response,function(key,value)
                {
                    employee_data += '<tr>';
                    employee_data += '<td>'+value.name+'</td>';
                    employee_data += '<td>'+value.start_time+'</td>';
                    employee_data += '<td>'+value.end_time+'</td>';
                    employee_data += '<td>'+value.duration+'</td>';
                    employee_data += '<td>'+value.tlp+'</td>';
                    employee_data += '<td><audio controls>\n' +
                        '<source src=\"'+value.record_url+'\" type="audio/flac">\n' +
                        '</audio></td>';

                    employee_data += '<tr>';
                });
                // $('#employee_table').append(employee_data);
                document.getElementById("employee_table").innerHTML = employee_data;
                console.log(2);
                $(".loader-block").hide();


            });
        }

    </script>
    <script>
        $(document).ready(function () {
            $('#datacetak').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            })
        })
    </script>
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop