@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.dataTables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/buttons.dataTables.min.css') }}">
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div class="col-md-12">

                    <div class="sub-title">
                        {{ Form::open(array('action' => 'Admin\HistoryOnlineController@rekap', 'method' => 'post')) }}

                        <input type="date" class="" name="starttime" placeholder="hdsafds"> - <input type="date" class="" name="endtime">
                        <select name="ustad">
                            @foreach ($ustad as $row)
                                <option value="{{$row->id}}">{{$row->name}}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-mini btn-info" type="submit"><i class="fa fa-print"></i>Submit</button>
                        <button class="btn btn-mini btn-info float-right" type="submit">Cetak Semua Ustad</button>
                        {{ Form::close() }}

                    </div>
                    <!-- Nav tabs -->

                    <!-- Tab panes -->
                    <div class="tab-content card-block">

                        <table class="table table-striped table-bordered nowrap datatable" id="datacetak">

                            <thead>
                            <tr>
                                <td colspan="5">
                                    Ustad
                                </td>
                            </tr>
                            <tr>
                                <th>Online</th>
                                <th>Offline</th>
                                <th>Total waktu Online</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $tampung = array();
                            array_push($tampung,'00:00:00');
                            ?>

                            @foreach ($ustadonline as $row1)

                                <tr>
                                    <td>
                                        {{ $row1->start_time }}
                                    </td>
                                    <td>
                                        {{ $row1->end_time }}
                                    </td>
                                    <td>
                                        {{ $row1->total_time }}

                                    </td>
                                    <?php
                                        if(empty($row1->total_time)){
                                            array_push($tampung,'00:00:00');
                                        }
                                        else{
                                            array_push($tampung,$row1->total_time);
                                        }

                                    ?>


                                </tr>

                            @endforeach
                            <tr>
                                <td>
                                    TOTAL
                                </td>
                                <td>
                                    =
                                </td>
                                <td>

                                    <?php
                                    if(empty($tampung)){
                                        echo 0;
                                    }
                                    else{
                                        $all_seconds = 0;
                                        foreach ($tampung as $time) {
                                            list($hour, $minute, $second) = explode(':', $time);
                                            $hour = (float)$hour;
                                            $minute = (float)$minute;
                                            $second = (float)$second;
                                            $all_seconds += $hour * 3600;
                                            $all_seconds += $minute * 60; $all_seconds += $second;
                                        }
                                        $total_minutes = floor($all_seconds/60);
                                        $seconds = $all_seconds % 60;
                                        $hours = floor($total_minutes / 60);
                                        $minutes = $total_minutes % 60;

                                        if($hours < 10){
                                            $hours = '0'.$hours;
                                        }
                                        if($seconds < 10){
                                            $seconds = '0'.$seconds;
                                        }
                                        if($minutes < 10){
                                            $minutes = '0'.$minutes;
                                        }
                                        $jam = $hours.':'.$minutes.':'.$seconds;
                                        echo $jam;

//                            print_r ($tampung);
                                    }



                                    ?>

                                </td>


                                <td>
                                    Jam
                                </td>
                            </tr>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ URL::asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/js/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('/js/vfs_fonts.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#datacetak').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            })

        })
    </script>
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop