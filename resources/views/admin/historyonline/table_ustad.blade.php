
<table class="table table-striped table-bordered nowrap datatable" id="datacetak">
    <thead>
    <tr>
        <td colspan="5">
            Ustad
        </td>
    </tr>
    <tr>
        <th>Online</th>
        <th>Offline</th>
        <th>Total waktu Online</th>
        <th>Detail</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $tampung = array();
    array_push($tampung,'00:00:00');
    ?>

    @foreach ($ustadonline as $row1)

        <tr>
            <td>
                {{ $row1->start_time }}
            </td>
            <td>
                {{ $row1->end_time }}
            </td>
            <td>
                {{ $row1->total_time }}

            </td>
            <?php
            if(empty($row1->total_time)){
                array_push($tampung,'00:00:00');
            }
            else{
                array_push($tampung,$row1->total_time);
            }

            ?>

            <td>
                {{--<a href="#default-Modal{{$row1->id_h_ustad}}" data-toggle="modal" data-target="#default-Modal{{$row1->id_h_ustad}}"><i class="fa fa-file"></i></a>--}}
                <button class="btn btn-mini" href="#default-Modal-rekaman" data-toggle="modal" data-target="#default-Modal-rekaman" onclick="get_data({{$row1->id_h_ustad}})">Detail</button>

            </td>
        </tr>

    @endforeach
    <tr>
        <td>
            TOTAL
        </td>
        <td>
            =
        </td>
        <td>

            <?php
            if(empty($tampung)){
                echo 0;
            }
            else{
                $all_seconds = 0;
                foreach ($tampung as $time) {
                    list($hour, $minute, $second) = explode(':', $time);
                    $hour = (float)$hour;
                    $minute = (float)$minute;
                    $second = (float)$second;
                    $all_seconds += $hour * 3600;
                    $all_seconds += $minute * 60; $all_seconds += $second;
                }
                $total_minutes = floor($all_seconds/60);
                $seconds = $all_seconds % 60;
                $hours = floor($total_minutes / 60);
                $minutes = $total_minutes % 60;

                if($hours < 10){
                    $hours = '0'.$hours;
                }
                if($seconds < 10){
                    $seconds = '0'.$seconds;
                }
                if($minutes < 10){
                    $minutes = '0'.$minutes;
                }
                $jam = $hours.':'.$minutes.':'.$seconds;
                echo $jam;
//                            print_r ($tampung);
            }
            ?>
        </td>


        <td>
            Jam
        </td>
    </tr>

    </tbody>
</table>
<div class="modal fade moda" id="default-Modal-rekaman" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Daftar Rekaman</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="loader" class="loader-block">
                    <svg id="loader2" viewBox="0 0 100 100">
                        <circle id="circle-loader2" cx="50" cy="50" r="45"></circle>
                    </svg>
                </div>
                <div class="table-responsive dt-responsive">

                    <table class="table table-striped table-bordered nowrap datatable">
                        <thead>
                        <tr>
                            <th>Jamaah</th>
                            <th>Online</th>
                            <th>Offline</th>
                            <th>Total waktu Telepon</th>
                            <th>Tlpn</th>
                            <th>Recording</th>
                        </tr>
                        </thead>
                        <tbody id="employee_table">

                        </tbody>
                    </table>
                </div>

            </div>
            <div class="modal-footer">

                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        <button type="button" class="btn btn-mini btn-danger waves-effect " data-dismiss="modal">Close</button>

                        {{--<button class="btn btn-sm btn-primary" type="submit">Save</button>--}}
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
