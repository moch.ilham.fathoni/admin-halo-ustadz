@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <!-- Modal -->

                    {!! Form::model($praytime, array('files' => true, 'class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'post', 'action' => array('Admin\ImagePrayTimeController@update', $praytime->id))) !!}

                    <div class="form-group">
                        {!! Form::label('name', 'Nama*', array('class'=>'control-label')) !!}
                        <div class="col-md-12">
                            {!! Form::text('name', old('name',$praytime->name), array('class'=>'form-control form-control-primary')) !!}

                        </div>
                    </div><div class="form-group">
                        {!! Form::label('image', 'image', array('class'=>'control-label')) !!}
                        <div class="col-md-12">
                            {!! Form::file('image') !!}
                            {!! Form::hidden('image_w', 4096) !!}
                            {!! Form::hidden('image_h', 4096) !!}

                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12 col-sm-offset-2">
                            {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
                            {!! link_to_route('content.artikel.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-sm btn-default')) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
            </div>

        </div>
    </div>
    </div>
@endsection
