@extends('admin.layouts.master')
@section('css')

@endsection
@section('content')
<div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div class="sub-title">
                    {{ Form::open(array('action' => 'Admin\JamaahController@getemail', 'method' => 'post')) }}
                    <input type="text" name="search" id="search" placeholder="email"> <button class="btn btn-mini btn-info" type="submit"><i class="fa fa-search"></i></i>Submit</button>
                    {{ Form::close() }}
                </div>
                    @if ($ustad->count())

                    <div id="table_data">
                    @include('admin.jamaah.load')
                    </div>

            <div class="row">
                <div class="col-xs-12">
                    <button class="btn btn-sm btn-danger" id="delete">
                        {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                    </button>
                </div>
            </div>
            {!! Form::open(['route' => config('quickadmin.route').'.ustad.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                <input type="hidden" id="send" name="toDelete">
            {!! Form::close() !!}

	</div>

@else
    {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
@endif
 </div>

            </div>
        </div>
    </div>
<div class="row">

</div>
@endsection

@section('javascript')
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>--}}
    {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
    <script>
        $(document).ready(function(){

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                $('#table_data table').css( 'color', '#ffffff');
                $('#table_data').append('<img style="position: absolute; left: 50%; top: 50%; z-index: 100000;" src="{{ asset('ablepro/assets/images/loading.gif') }}" />');
                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });
            function fetch_data(page)
            {
                $.ajax({
                    url:"/admin/jamaah/page?page="+page,
                }).done(function (data) {
                    $('#table_data').html(data);
                    console.log(data);
                }).fail(function () {
                    // alert('Jamaah could not be loaded.');
                    console.log("gagal");
                // });
                //     success:function(data)
                //     {
                //         $('#table_data').html(data);
                //     },
                //     fail:function () {
                //         alert('Articles could not be loaded.');
                //     }
                });
            }

        });


    </script>
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop