<div class="table-responsive">
    <table class="table table-striped table-bordered nowrap datatable" id="datatable">
        <thead>
        <tr>
            <th>
                {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
            </th>
            <th>ID Jamaah</th>
            <th>Nama Jamaah</th>
            <th>Username</th>
            <th>Vox ID</th>
            <th>Tlp</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody class="ustad">
        @foreach ($ustad as $row)
            <tr >
                <td>
                    {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                </td>
                <td>{{ $row->email }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->username }}</td>
                <td>{{ $row->vox_id }}</td>
                <td>{{ $row->tlp }}</td>

                <td><span class="pcoded-badge label
                            @if ($row->is_active == 0)
                            label-success">Active
                        @else
                            label-danger">Blacklist
                        @endif
                            </span>
                </td>

                <td>

                    {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array('jamaah.destroy', $row->id))) !!}
                    {!! Form::submit(trans('quickadmin::templates.templates-view_index-delete'), array('class' => 'btn btn-mini btn-danger')) !!}
                    {!! Form::close() !!}
                    @if($row->is_active == 0)


                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');", 'action' => 'Admin\JamaahController@blackList')) !!}
                        <input type="hidden" value="{{ $row->id }}" name="id">
                        <input type="hidden" value="1" name="is_active">
                        <button type="submit" class="btn btn-mini btn-secondary">Deactive</button>
                        {!! Form::close() !!}
                    @else
                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');", 'action' => 'Admin\JamaahController@blackList')) !!}
                        <input type="hidden" value="{{ $row->id }}" name="id">
                        <input type="hidden" value="0" name="is_active">
                        <button type="submit" class="btn btn-mini btn-warning">Active</button>
                        {!! Form::close() !!}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>

<nav aria-label="Page navigation example">
{{ $ustad->links('vendor.pagination.bootstrap-4') }}
    <div id="loading"></div>
</nav>