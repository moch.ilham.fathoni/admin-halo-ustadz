@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <!-- Modal -->

                    <form method="POST" action="{{ route('admin.logorekaman.simpan') }}" enctype="multipart/form-data" accept-charset="UTF-8">
                        <img width="200px" class="img-thumbnail" src="{{ url('public/uploads/file/logorekaman/logo_rekaman.jpg') }}">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            {!! Form::label('image', 'image', array('class'=>'control-label')) !!}
                            <div class="col-md-12">
                                {!! Form::file('image') !!}
                                {!! Form::hidden('image_w', 4096) !!}
                                {!! Form::hidden('image_h', 4096) !!}
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-12 col-sm-offset-2">
                            <input type="submit" class="btn btn-primary">
                            {!! link_to_route(config('quickadmin.route').'.logorekaman.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-sm btn-default')) !!}
                        </div>
                    </div>

                        <form>
            </div>

        </div>
    </div>
    </div>
@endsection
