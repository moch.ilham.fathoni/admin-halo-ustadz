@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <!-- Modal -->


                        <div class="table-responsive dt-responsive">
                            <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Deskripsi</th>
                                    <th>Image</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>

                                </tbody>
                            </table>


                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-sm btn-danger" id="delete">
                                        {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                                    </button>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'content.artikel.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                            <input type="hidden" id="send" name="toDelete">
                            {!! Form::close() !!}
                        </div>
                </div>

            </div>

        </div>
    </div>
    </div>
@endsection
