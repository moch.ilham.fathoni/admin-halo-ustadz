<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="">

        </div>
        <div class="pcoded-navigation-label">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">

            <li class="pcoded-hasmenu">
                <a class="waves-effect waves-dark">
                    <i class="fa"></i>
                    <i class="fa"></i>
                    <span class="title">Kontent Harian</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="pcoded-submenu">

                    <li class="active active-sub">
                        <a href="{{ route('content.artikel.index') }}" class="waves-effect waves-dark">
                            <i class="fa "></i>
                            <span class="title">
                                Artikel
                            </span>
                        </a>
                    </li>

                </ul>
                <ul class="pcoded-submenu">

                    <li class="active active-sub">
                        <a href="{{ route('content.doaharian.index') }}" class="waves-effect waves-dark">
                            <i class="fa "></i>
                            <span class="title">
                                Doa Harian
                            </span>
                        </a>
                    </li>

                </ul>
                <ul class="pcoded-submenu">
                    <li class="active active-sub">
                        <a href="{{ route('content.hadits.index') }}" class="waves-effect waves-dark">
                            <i class="fa "></i>
                            <span class="title">
                                Hadits Harian
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="pcoded-submenu">
                    <li class="active active-sub">
                        <a href="{{ route('content.event.index') }}" class="waves-effect waves-dark">
                            <i class="fa "></i>
                            <span class="title">
                                Event
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
