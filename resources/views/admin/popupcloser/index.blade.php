@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.3.1.js"
            integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
            crossorigin="anonymous"></script>
@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <!-- Modal -->
                    <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Edit</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    {!! Form::open(array('files' => true, 'action' => 'Admin\HaditsController@store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}
                                    <div class="form-group">
                                        {!! Form::label('name', 'Judul*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::text('name', old('name'), array('class'=>'form-control form-control-primary')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        {!! Form::label('image', 'image*', array('class'=>'control-label')) !!}
                                        <div class="col-md-12">
                                            {!! Form::file('image') !!}
                                            {!! Form::hidden('image_w', 4096) !!}
                                            {!! Form::hidden('image_h', 4096) !!}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-offset-2">
                                            {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-primary')) !!}
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                    </div>
                    @if ($popupcloser->count())
                        <div class="table-responsive dt-responsive">
                            <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Deskripsi</th>
                                    <th>Image</th>
                                    <th>&nbsp;</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach ($popupcloser as $row)
                                    <tr>

                                        <td>{{ $row->title }}</td>
                                        <td>{!! $row->deskripsi !!}</td>
                                        <td><img class="img-responsive" width="200px" src="{{ $row->image }}"></td>
                                        <td>
                                           <a href="{{ route('admin.popupcloser.edit',$row->id) }}"><button class="btn btn-sm btn-info" value="edit">Edit</button></a>
                                        </td>
                                    </tr>

                                @endforeach
                                </tbody>
                            </table>


                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-sm btn-danger" id="delete">
                                        {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                                    </button>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'content.artikel.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                            <input type="hidden" id="send" name="toDelete">
                            {!! Form::close() !!}
                        </div>
                </div>
                @else
                    {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
                @endif
            </div>

        </div>
    </div>
    </div>
@endsection
