@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>Recording List</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    {!! link_to_route(config('quickadmin.route').'.recording.create', trans('quickadmin::templates.templates-view_index-add_new') , null, array('class' => 'btn btn-success')) !!}
                </div>


                @if ($recording->count())


                    <div class="table-responsive dt-responsive">

                        <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                            <thead>
                            <tr>
                                <th>
                                    {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                                </th>
                                <th>Judul</th>
                                <th>Pertanyaan</th>
                                <th>Ustad</th>
                                <th>Recording</th>

                                <th>&nbsp;</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($recording as $row)
                                <tr>
                                    <td>
                                        {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                                    </td>
                                    <td>{{ $row->judul }}</td>
                                    <td>{!! $row->pertanyaan !!}</td>
                                    <td>{{ $row->id_ustad }}</td>
                                    <td><audio controls>
                                            <source src="{{ $row->recording_url }}" type="audio/waf">
                                            <source src="{{ $row->recording_url }}" type="audio/flac">
                                            <source src="{{ $row->recording_url }}" type="audio/mp3">

                                        </audio></td>

                                    <td>

                                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.recording.destroy', $row->id))) !!}
                                        {!! Form::submit(trans('quickadmin::templates.templates-view_index-delete'), array('class' => 'btn btn-sm btn-danger')) !!}
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-sm btn-danger" id="delete">
                                    {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                                </button>
                            </div>
                        </div>
                        {!! Form::open(['route' => config('quickadmin.route').'.event.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                        <input type="hidden" id="send" name="toDelete">
                        {!! Form::close() !!}
                    </div>
            </div>
            @else
                {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
            @endif
        </div>

    </div>
    </div>
    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop