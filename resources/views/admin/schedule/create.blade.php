@extends('admin.layouts.master')

@section('content')
 <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
<div class="row">
    <div class="col-md-12 col-sm-offset-2">

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

                {!! Form::open(array('route' => config('quickadmin.route').'.schedule.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                <div class="form-group">
                    {!! Form::label('id_ustad', 'ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('user_id', old('id_ustad'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('id_ustad', 'ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('user_app_id', old('id_ustad'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('hari',     'Hari*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('hari', old('hari'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('start_time', 'Jam Mulai*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('start_time', old('start_time'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('end_time', 'Jam Akhir*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('end_time', old('end_time'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
</div>
</div>
</div>
@endsection