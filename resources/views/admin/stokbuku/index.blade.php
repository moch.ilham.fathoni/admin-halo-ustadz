@extends('admin.layouts.master')
@section('css')
    <script
            src="https://code.jquery.com/jquery-3.4.1.js"
            integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}">
    <script type="text/javascript" src="{{ URL::asset('ablepro/bower_components/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModalScrollable">
                        Add New
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                        <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalScrollableTitle">Add New</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                </div>

                            </div>
                        </div>
                    </div>

                        <div class="table-responsive dt-responsive">
                            <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        Buku
                                    </th>
                                    <th>Stok</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($stokbuku as $raw)
                                    <tr>
                                        <td>{{ number_format($raw->jenis_buku) - number_format($sisa[0]->tafsir)  }}</td>
                                        <td>{{ $raw->stok }}</td>
                                        <td>{{ $raw->stok }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>


                            <div class="row">
                                <div class="col-xs-12">
                                    <button class="btn btn-sm btn-danger" id="delete">
                                        {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                                    </button>
                                </div>
                            </div>
                            {!! Form::open(['route' => 'content.artikel.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                            <input type="hidden" id="send" name="toDelete">
                            {!! Form::close() !!}
                        </div>
                </div>

            </div>

        </div>
    </div>
    </div>
    <script>
        $(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
        function changestatus() {
            status = $("#status").val();
            if(status == 1){
                $("#hidden_post").css('display','block');
            }
            else if(status == 0){
                $("#hidden_post").css('display','none');
            }
        }
    </script>
@endsection

@section('javascript')
    <!-- Custom js -->
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });


        function editdata(id) {
            getdata(id);


        }
        function getdata(id) {
            var url="{{ url('/') }}"+"/api/hadits/"+id;
            var settings = {
                "async": true,
                "crossDomain": true,
                "method": "GET",

                "headers": {
                    "cache-control": "no-cache",
                    "Postman-Token": "30d21590-8c00-4a63-b09a-daffa2bddac6",
                    "Access-Control-Allow-Origin": "*",
                    'Access-Control-Allow-Headers': 'Origin, Content-Type, Accept, Authorization, X-Request-With',
                },

            }
            $.getJSON(url,settings, function(response)
            {
                $('#title').val(response.title);
                $('#riwayat').val(response.content_riwayat);
                $('#no_hadits').val(response.no_hadits);
                var arab = response.content_arab;
                var text = response.content_text;
                $(this).data("id","content_text").html();
                document.getElementById("content_arab").innerText = arab;
                document.getElementById("content_text").innerText = text;


                console.log(response);

                $('#edit').modal('show');

            });

        }

    </script>
@stop
