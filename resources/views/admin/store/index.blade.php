@extends('admin.layouts.master')
@section('css')
    <link rel="stylesheet" href="{{ URL::asset('ablepro/assets/css/image_zoom.css') }}" type="text/css" media="all">

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div class="sub-title">
                    {{ Form::open(array('action' => 'Admin\JamaahController@getemail', 'method' => 'post')) }}
                    <input type="text" name="search" id="search" placeholder="email"> <button class="btn btn-mini btn-info" type="submit"><i class="fa fa-search"></i></i>Submit</button>
                    {{ Form::close() }}
                </div>
                @if ($store->count())

                    <div id="table_data">
                        <div class="table-responsive dt-responsive">
                            <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                                <thead>
                                <tr>
                                    <th>
                                        {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                                    </th>
                                    <th>Nama</th>
                                    <th>Tlpn</th>
                                    <th>Tafsir</th>
                                    <th>Haji</th>
                                    <th>Tauhid</th>
                                    <th>Kategori</th>
                                    <th>Bayar</th>
                                    <th>Keterangan</th>
                                    <th>Bukti TF</th>
                                    <th>Tanggal</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody class="ustad">
                                @foreach ($store as $row)
                                    <tr >
                                        <td>
                                            {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                                        </td>
                                        <td>{{ $row->nama }}</td>
                                        <td><a target="_blank" href="https://api.whatsapp.com/send?phone={{ '62'.substr($row->tlpn,1)}}&text=Assalamu’alaikum Warahmatullahi Wabarakaatuh%0A%0A%2AReminder Pemesanan Buku%2A%0ANama%20:%20{{ $row->nama }}%0AAlamat%20:%20{{ $row->alamat_lengkap }}%0ANO. HP%20:%20{{ $row->tlpn }}%0APemesanan Buku Tafsir Juz 'Amma%20:%20{{ $row->tafsir }} buku%0APemesanan Buku Bekal Haji%20:%20{{ $row->bekal_haji }} buku%0APemesanan Buku Tauhid%20:%20{{ $row->tauhid }} buku%0AOngkos Kirim%20:%20{{ "Rp " . number_format($row->harga_ongkir,0,',','.') }}%0ATotal pembayaran%20:%20*{{ "Rp " . number_format($row->total_harga,0,',','.') }}*%0A%0A*Apabila Sudah Melakukan pembayaran, silahkan Upload Bukti pembayaran, di link berikut ini*%0A%0AKunjungi Kami di %3A https%3A%2F%2Fapp%2Ehaloustadz%2Eid%2Fbookstore">{{ '62'.substr($row->tlpn,1)}}</a></td>
                                        <td>{{ $row->tafsir }}</td>
                                        <td>{{ $row->bekal_haji }}</td>
                                        <td>{{ $row->tauhid }}</td>
                                        <td>
                                            @if($row->kategori >= 1)
                                                <label class="label label-success">Tebar Buku</label>
                                            @else
                                                <label class="label label-danger">Beli Buku</label>

                                            @endif
                                        </td>
                                        <td>{{ $row->total_harga }}</td>
                                        <td>{{ $row->pekerjaan }}</td>
                                        <td><img width="200px" src="{{ $row->bukti_tf }}">
                                        </td>
                                        <td>{{ $row->updated_at }}
                                        </td>
                                        <td>
                                            <input type="button" href="#" class="btn btn-mini btn-secondary" onclick="modal({{ $row->id }})" value="Detail"><input type="button"  id="selesai{{ $row->id }}" onclick="sudahtf({{ $row->id }})" class="btn btn-mini btn-success" value="Sudah TF">
                                            {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array('admin.order.destroy'))) !!}
                                            <input hidden name="id" value="{{ $row->id }}">
                                            {!! Form::submit(trans('quickadmin::templates.templates-view_index-delete'), array('class' => 'btn btn-mini btn-danger')) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>



                        <nav aria-label="Page navigation example">
                            {{--{{ $store->links('vendor.pagination.bootstrap-4') }}--}}
                            <div id="loading"></div>
                        </nav>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-sm btn-danger" id="delete">
                                {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                            </button>
                        </div>
                    </div>
                    {!! Form::open(['route' =>'admin.order.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                    <input type="hidden" id="send" name="toDelete">
                    {!! Form::close() !!}

            </div>

            @else
                {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
            @endif
        </div>

    </div>
    </div>
    </div>
    <div id="mymodal" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p >
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Keterangan</th>
                                <th>Hasil</th>
                            </tr>
                            </thead>
                            <tbody id="content">

                            </tbody>
                        </table>
                    </div>
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">

    </div>
@endsection

@section('javascript')

    <script src="{{ URL::asset('/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('/js/buttons.html5.min.js') }}"></script>
    <script src="{{ URL::asset('/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jszip.min.js') }}"></script>
    <script src="{{ URL::asset('/js/pdfmake.min.js') }}"></script>
    <script src="{{ URL::asset('/js/vfs_fonts.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#datacetak').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ]
            });



        });
        function notify(title, msg){
            var from = 'bottom';
            var align = 'right';
            var icon = '';
            var type = 'inverse';
            var animIn = '';
            var animOut = '';
            $.growl({
                icon: icon,
                title: title,
                message: msg,
                url: ''
            },{
                element: 'body',
                type: type,
                allow_dismiss: true,
                placement: {
                    from: from,
                    align: align
                },
                offset: {
                    x: 30,
                    y: 30
                },
                spacing: 10,
                z_index: 999999,
                delay: 2500,
                timer: 1000,
                url_target: '_blank',
                mouse_over: false,
                animate: {
                    enter: animIn,
                    exit: animOut
                },
                icon_type: 'class',
                template: '<div data-growl="container" class="alert" role="alert">' +
                '<button type="button" class="close" data-growl="dismiss">' +
                '<span aria-hidden="true">&times;</span>' +
                '<span class="sr-only">Close</span>' +
                '</button>' +
                '<span data-growl="icon"></span>' +
                '<span data-growl="title"></span>' +
                '<span data-growl="message"></span>' +
                '<a href="#" data-growl="url"></a>' +
                '</div>'
            });
        };
        var url = '{{ url('/') }}';
        function sudahtf(id) {
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url+"/api/sudahtf",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache",
                    "Postman-Token": "9c1dfe9f-3a2c-4d4a-be0c-b2709657b46b"
                },
                "data": {
                    "id": id,
                    "status": '2'
                }
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
                document.getElementById('selesai'+id).disabled = true;
                document.getElementById("selesai"+id).value = "Selesai";
                notify("Sukses","Bukti TF sudah di periksa untuk di lanjutkan Packing");

            });
        }
        function modal(id) {

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": url+"/api/detail",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache",
                    "Postman-Token": "9c1dfe9f-3a2c-4d4a-be0c-b2709657b46b"
                },
                "data": {
                    "id": id
                }
            }

            $.ajax(settings).done(function (response) {
                console.log(response.result[0]);
                text =
                    "<tr><td>Nama</td>\n" +
                    "<td>"+response.result[0].nama+"</td></tr>\n"+
                    "<tr><td>Tlpn</td>\n" +
                    "<td>"+response.result[0].tlpn+"</td></tr>\n"+
                    "<tr><td>Jumlah Buku Tafsir</td>\n" +
                    "<td>"+response.result[0].tafsir+"</td></tr>\n"+
                    "<tr><td>Jumlah Buku Bekal Haji</td>\n" +
                    "<td>"+response.result[0].bekal_haji+"</td></tr>\n"+
                    "<tr><td>Jumlah Buku tauhid</td>\n" +
                    "<td>"+response.result[0].tauhid+"</td></tr>\n"+
                    "<tr><td>Alamat Lengkap</td>\n" +
                    "<td calass=\"text-nowrap\">"+response.result[0].alamat_lengkap+"</td></tr>\n"+
                    "<tr><td>Harga Ongkir</td>\n" +
                    "<td>"+response.result[0].harga_ongkir+"</td></tr>\n"+
                    "<tr><td>kecamatan</td>\n" +
                    "<td>"+response.result[0].kecamatan+"</td></tr>\n"+
                    "<tr><td>kota</td>\n" +
                    "<td>"+response.result[0].kota+"</td></tr>\n"+
                    "<tr><td>kurir</td>\n" +
                    "<td>"+response.result[0].kurir+"</td></tr>\n"+
                    "<tr><td>provinsi</td>\n" +
                    "<td>"+response.result[0].provinsi+"</td></tr>\n"+
                    "<tr><td>total harga</td>\n" +
                    "<td>"+response.result[0].total_harga+"</td></tr>\n"+
                    "<tr><td>Tgl Order</td>\n" +
                    "<td>"+response.result[0].created_at+"</td></tr>\n"+
                    "<tr><td>Tgl Proses</td>\n" +
                    "<td>"+response.result[0].updated_at+"</td></tr>\n"+
                    "<tr><td>Resi</td>\n" +
                    "<td>"+response.result[0].resi+"</td></tr>\n"+
                    "<tr><td>Bukti TF</td>\n" +
                    "<td><image src=\""+response.result[0].bukti_tf+"\" width=\"300px\"></td></tr>\n";
                document.getElementById("content").innerHTML = text;
                $('#mymodal').modal('show');
            });
        }

    </script>
    <script>
        $(document).ready(function(){

            $(document).on('click', '.pagination a', function(event){
                event.preventDefault();
                $('#table_data table').css( 'color', '#ffffff');
                $('#table_data').append('<img style="position: absolute; left: 50%; top: 50%; z-index: 100000;" src="{{ asset('ablepro/assets/images/loading.gif') }}" />');
                var page = $(this).attr('href').split('page=')[1];
                fetch_data(page);
            });
            function fetch_data(page)
            {
                $.ajax({
                    url:"/admin/jamaah/page?page="+page,
                }).done(function (data) {
                    $('#table_data').html(data);
                    console.log(data);
                }).fail(function () {
                    // alert('Jamaah could not be loaded.');
                    console.log("gagal");
                    // });
                    //     success:function(data)
                    //     {
                    //         $('#table_data').html(data);
                    //     },
                    //     fail:function () {
                    //         alert('Articles could not be loaded.');
                    //     }
                });
            }

        });


    </script>
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>
@stop
