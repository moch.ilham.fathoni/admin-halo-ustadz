@extends('admin.layouts.master')

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(array('route' => 'tafsirjuzama.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                <div class="form-group">
                    {!! Form::label('page_name', 'page_name*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('page_name', old('page_name'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('title', 'title*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('title', old('title'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('content', 'Isi content*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('content', old('content'), array('class'=>'form-control ckeditor')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('tafsir_surah_id', 'Jam tafsir_surah_id*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('tafsir_surah_id', old('tafsir_surah_id'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection