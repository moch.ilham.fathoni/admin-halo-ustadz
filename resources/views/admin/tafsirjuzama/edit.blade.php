@extends('admin.layouts.master')

@section('content')
<div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h5>
            </div>
            <div class="card-block">
<div class="row">
    <div class="col-md-12 col-sm-offset-2">

        @if ($errors->any())
        	<div class="alert alert-danger">
        	    <ul>
                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                </ul>
        	</div>
        @endif
    </div>
</div>

{!! Form::model($schedule, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.schedule.update', $schedule->id))) !!}

<div class="form-group">
    {!! Form::label('id_ustad', 'ustad*', array('class'=>'control-label')) !!}
    <div class="col-md-12">
        {!! Form::text('id_ustad', old('id_ustad',$schedule->id_ustad), array('class'=>'form-control form-control-primary')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('hari', 'Hari*', array('class'=>'control-label')) !!}
    <div class="col-md-12">
        {!! Form::text('hari', old('hari',$schedule->hari), array('class'=>'form-control form-control-primary')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('start_time', 'Jam Mulai*', array('class'=>'control-label')) !!}
    <div class="col-md-12">
        {!! Form::text('start_time', old('start_time',$schedule->start_time), array('class'=>'form-control form-control-primary')) !!}
        
    </div>
</div><div class="form-group">
    {!! Form::label('end_time', 'Jam Akhir*', array('class'=>'control-label')) !!}
    <div class="col-md-12">
        {!! Form::text('end_time', old('end_time',$schedule->end_time), array('class'=>'form-control form-control-primary')) !!}
        
    </div>
</div>

<div class="form-group">
    <div class="col-md-12 col-sm-offset-2">
      {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
      {!! link_to_route(config('quickadmin.route').'.schedule.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-sm btn-default')) !!}
    </div>
</div>

{!! Form::close() !!}
</div>
</div>
</div>
@endsection