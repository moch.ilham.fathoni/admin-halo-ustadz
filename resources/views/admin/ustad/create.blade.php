@extends('admin.layouts.master')
@section('css')

    <link rel="stylesheet" href="{{ URL::asset('ablepro/bower_components/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/multiselect/css/multi-select.css')}}" />

    <link rel="stylesheet" href="{{ URL::asset('css/timedropper.css') }}" />

@endsection

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_create-add_new') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(array('files' => true,'route' => config('quickadmin.route').'.ustad.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                <div class="form-group">
                    {!! Form::label('email', 'E-Mail*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('email', old('E-Mail'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('username', 'Username Ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('username', old('username'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('name', 'Nama Ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('name', old('nama_ustad'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>


                <div class="form-group">
                    {!! Form::label('Asal', 'Asal*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('asal', old('Asal'), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('pendidikan', 'Riwayar Pendidikan*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('pendidikan', old('Riwayar Pendidikan'), array('class'=>'form-control my-editor')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password_ustad', 'Password*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::password('password_ustad', array('class'=>'form-control form-control-primary','id'=>'psw','min'=>'8')) !!}

                    </div>

                </div>

                <input type="hidden" name="user_category" value="1">
                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit( trans('quickadmin::templates.templates-view_create-create') , array('class' => 'btn btn-sm btn-primary')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
@section('javascrip')

    <script>
        var myInput = document.getElementById("psw");
        var letter = document.getElementById("letter");
        var capital = document.getElementById("capital");
        var number = document.getElementById("number");
        var length = document.getElementById("length");

        // When the user clicks on the password field, show the message box
        myInput.onfocus = function() {
            document.getElementById("message").style.display = "block";
        }

        // When the user clicks outside of the password field, hide the message box
        myInput.onblur = function() {
            document.getElementById("message").style.display = "none";
        }

        // When the user starts to type something inside the password field
        myInput.onkeyup = function() {
            // Validate lowercase letters
            var lowerCaseLetters = /[a-z]/g;
            if(myInput.value.match(lowerCaseLetters)) {
                letter.classList.remove("invalid");
                letter.classList.add("valid");
            } else {
                letter.classList.remove("valid");
                letter.classList.add("invalid");
            }

            // Validate capital letters
            var upperCaseLetters = /[A-Z]/g;
            if(myInput.value.match(upperCaseLetters)) {
                capital.classList.remove("invalid");
                capital.classList.add("valid");
            } else {
                capital.classList.remove("valid");
                capital.classList.add("invalid");
            }

            // Validate numbers
            var numbers = /[0-9]/g;
            if(myInput.value.match(numbers)) {
                number.classList.remove("invalid");
                number.classList.add("valid");
            } else {
                number.classList.remove("valid");
                number.classList.add("invalid");
            }

            // Validate length
            if(myInput.value.length >= 8) {
                length.classList.remove("invalid");
                length.classList.add("valid");
            } else {
                length.classList.remove("valid");
                length.classList.add("invalid");
            }
        }
    </script>
    <script type="text/javascript" src="{{ URL::asset('js/timedropper.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/select2/js/select2.full.min.js')}}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js')}}">
    </script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//assets/js/jquery.quicksearch.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{ URL::asset('ablepro/assets/pages/advance-elements/select2-custom.js')}}"></script>

@endsection
