@extends('admin.layouts.master')

@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_edit-edit') }}</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('<li class="error">:message</li>')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::model($ustad, array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'route' => array(config('quickadmin.route').'.ustad.update', $ustad->id))) !!}

                <div class="form-group">
                    {!! Form::label('email', 'E-Mail*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('id_ustad', old('id_ustad',$ustad->email), array('class'=>'form-control form-control-primary')) !!}
                    </div>
                </div><div class="form-group">
                    {!! Form::label('name', 'Nama Ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('name', old( 'nama_ustad',$ustad->name), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('username', 'usernama Ustad*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('username', old( 'nama_ustad',$ustad->username), array('class'=>'form-control form-control-primary','disabled')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('tempat_tgl_lahir', 'tempat tgl lahir*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('tempat_tgl_lahir', old( 'tempat tgl lahir',$ustad->tempat_tgl_lahir), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('domisili', 'Domisili*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('domisili', old('domisili',$ustad->name), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('riwayat_pend', 'Riwayar Pendidikan*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('riwayat_pend', old('riwayat_pend',$ustad->riwayat_pend), array('class'=>'form-control my-editor')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('karya_tulis', 'Karya Tulis*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('karya_tulis', old('karya_tulis',$ustad->karya_tulis), array('class'=>'form-control my-editor')) !!}

                    </div>
                </div><div class="form-group">
                    {!! Form::label('guru', 'Guru*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::textarea('guru', old('guru',$ustad->guru), array('class'=>'form-control my-editor')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('bidang_keilmuan', 'Bidang Keilmuan*', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        {!! Form::text('bidang_keilmuan', old( 'Bidang Keilmuan',$ustad->bidang_keilmuan), array('class'=>'form-control form-control-primary')) !!}

                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('password_ustad', 'Password', array('class'=>'control-label')) !!}
                    <div class="col-md-12">
                        <input type="password" value="{{$ustad->password_ustad}}" class="form-control form-control-primary" name="password_ustad">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
                        {!! link_to_route(config('quickadmin.route').'.ustad.index', trans('quickadmin::templates.templates-view_edit-cancel'), null, array('class' => 'btn btn-sm btn-default')) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection