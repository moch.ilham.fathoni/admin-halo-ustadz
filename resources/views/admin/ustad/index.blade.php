@extends('admin.layouts.master')
@section('css')
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <!-- Style.css -->
    <link rel="stylesheet" href="{{ URL::asset('ablepro/bower_components/select2/css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/bootstrap-multiselect/css/bootstrap-multiselect.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('ablepro/bower_components/multiselect/css/multi-select.css')}}" />

    <link rel="stylesheet" href="{{ URL::asset('css/timedropper.css') }}" />

@endsection
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>{{ trans('quickadmin::templates.templates-view_index-list') }}</h5>
            </div>
            <div class="card-block">
                <div id="nestable-menu" class="m-b-10">
                    {!! link_to_route(config('quickadmin.route').'.ustad.create', trans('quickadmin::templates.templates-view_index-add_new') , null, array('class' => 'btn btn-success')) !!}
                </div>
                @if ($ustad->count())

                    <div class="table-responsive dt-responsive">
                        <table class="table table-striped table-bordered nowrap datatable" id="datatable">
                            <thead>
                            <tr>
                                <th>
                                    {!! Form::checkbox('delete_all',1,false,['class' => 'mass']) !!}
                                </th>
                                <th>ID Ustad</th>
                                <th>Nama Ustad</th>
                                <th>Kategori</th>
                                <th>Jadwal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($ustad as $row)
                                <tr>
                                    <td>
                                        {!! Form::checkbox('del-'.$row->id,1,false,['class' => 'single','data-id'=> $row->id]) !!}
                                    </td>
                                    <td>{{ $row->email }}</td>
                                    <td>{{ $row->name }}</td>
                                    <td>
                                        @for($a = 0 ; $a<count($category) ; $a++)
                                            @if($category[$a]->user_app_id === $row->id )
                                                |{{ $category[$a]->nama_category }}|
                                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array('role_category.destroy', $category[$a]->id ))) !!}
                                                {{ method_field('DELETE') }}
                                                <button type="submit" class="btn" style='background-color:transparent;'><i class="fa fa-trash text-danger"></i></button>

                                                {!! Form::close() !!}

                                                <br>

                                            @endif
                                        @endfor
                                    </td>
                                    <td>

                                        @foreach($schedule as $jadwal)
                                            @if($jadwal->user_id == $row->id)
                                                {{$jadwal->hari}} : {{$jadwal->start_time}} - {{$jadwal->end_time}}
                                                {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'action' => array('Admin\ScheduleController@destroy', $jadwal->id))) !!}
                                                <a href="#default-Modal-edit{{$jadwal->id}}" data-toggle="modal" data-target="#default-Modal-edit{{$jadwal->id}}"><i class="fa fa-edit"></i></a>
                                                <button type="submit" class="btn" style='background-color:transparent;'><i class="fa fa-trash text-danger"></i></button>
                                                {!! Form::close() !!}
                                                <br>
                                                <div class="modal fade" id="default-Modal-edit{{$jadwal->id}}" tabindex="-1" role="dialog">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h4 class="modal-title">Jadawal Ustad</h4>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>


                                                            <div class="modal-body">
                                                                {!! Form::open(array('class' => 'form-horizontal', 'id' => 'form-with-validation', 'method' => 'PATCH', 'action' => array('Admin\ScheduleController@update', $jadwal->id))) !!}

                                                                <div class="form-group">
                                                                    {!! Form::label('hari', 'Hari*', array('class'=>'control-label')) !!}
                                                                    <div class="col-md-12">
                                                                        <select class="form-control" name="hari">
                                                                            <option value="{{$jadwal->hari}}">{{$jadwal->hari}}</option>
                                                                            <option value="Senin">Senin</option>
                                                                            <option value="Selasa">Selasa</option>
                                                                            <option value="Rabu">Rabu</option>
                                                                            <option value="Kamis">Kamis</option>
                                                                            <option value="Jumat">Jumat</option>
                                                                            <option value="Sabtu">Sabtu</option>
                                                                            <option value="Minggu">Minggu</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                {!! Form::label('Jam', 'Jam*', array('class'=>'control-label')) !!}
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col">
                                                                            <input style="color: black;" type="text" name="start_time" class="form-control form-control-primary" value="{{$jadwal->start_time}}" id="start_tme{{$row->id}}" />
                                                                        </div>

                                                                        sampai

                                                                        <div class="col">
                                                                            <input style="color: black;" type="text" name="end_time" class="form-control form-control-primary" value="{{$jadwal->end_time}}" id="end_tme{{$row->id}}" />
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                {!! Form::submit(trans('quickadmin::templates.templates-view_edit-update'), array('class' => 'btn btn-primary')) !!}
                                                                {!! Form::close() !!}
                                                            </div>
                                                            <div class="modal-footer">

                                                                <div class="form-group">
                                                                    <div class="col-md-12 col-sm-offset-2">
                                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

                                                                        {{--<button class="btn btn-sm btn-primary" type="submit">Save</button>--}}
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </div>
                                    @endif
                                    @endforeach
                                    <td><span class="pcoded-badge label
                            @if ($row->status == 1)
                                                label-success">Available
                                            @else
                                                label-danger">Not Available
                                            @endif
                            </span></td>
                                    <td>
                                        <button class="btn btn-mini btn-success" data-toggle="modal" data-target="#default-Modal01{{$row->id}}">Add Category</button>
                                        <div class="modal fade" id="default-Modal01{{$row->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Category Ustad</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>

                                                    <div class="modal-body">
                                                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'action' => 'Admin\RoleCategoryController@store')) !!}
                                                        <input type="hidden" name="category_id" value="{{$row->id}}">
                                                        <input type="hidden" name="user_app_id" value="{{$row->id}}">
                                                        <div class="form-group">
                                                            {!! Form::label('Category', 'Category*', array('class'=>'control-label')) !!}
                                                            <div class="col-md-12">
                                                                {!! Form::select('category_id', $categoryall, old('category_id'), array('class'=>'form-control')) !!}
                                                            </div>
                                                        </div>



                                                    </div>
                                                    <div class="modal-footer">

                                                        <div class="form-group">
                                                            <div class="col-md-12 col-sm-offset-2">
                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

                                                                <button class="btn btn-sm btn-primary" type="submit">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-mini btn-success" data-toggle="modal" data-target="#default-Modal{{$row->id}}">Add Schedule</button>
                                        <div class="modal fade" id="default-Modal{{$row->id}}" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Jadawal Ustad</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    {!! Form::open(array('route' => config('quickadmin.route').'.schedule.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

                                                    <div class="modal-body">

                                                        <input type="hidden" name="user_id" value="{{$row->id}}">
                                                        <input type="hidden" name="user_app_id" value="{{$row->id}}">
                                                        <div class="form-group">
                                                            {!! Form::label('hari', 'Hari*', array('class'=>'control-label')) !!}
                                                            <div class="col-md-12">
                                                                <select class="form-control" name="hari">
                                                                    <option value="Senin">Senin</option>
                                                                    <option value="Selasa">Selasa</option>
                                                                    <option value="Rabu">Rabu</option>
                                                                    <option value="Kamis">Kamis</option>
                                                                    <option value="Jumat">Jumat</option>
                                                                    <option value="Sabtu">Sabtu</option>
                                                                    <option value="Minggu">Minggu</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        {!! Form::label('Jam', 'Jam*', array('class'=>'control-label')) !!}
                                                        <div class="col-md-12">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <input style="color: black;" type="text" name="start_time" class="form-control form-control-primary" id="start_tme{{$row->id}}" />
                                                                </div>

                                                                sampai

                                                                <div class="col">
                                                                    <input style="color: black;" type="text" name="end_time" class="form-control form-control-primary" id="end_tme{{$row->id}}" />
                                                                </div>

                                                            </div>
                                                        </div>



                                                    </div>
                                                    <div class="modal-footer">

                                                        <div class="form-group">
                                                            <div class="col-md-12 col-sm-offset-2">
                                                                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Close</button>

                                                                <button class="btn btn-sm btn-primary" type="submit">Save</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    {!! Form::close() !!}

                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        {!! link_to_route(config('quickadmin.route').'.ustad.edit', trans('quickadmin::templates.templates-view_index-edit'), array($row->id), array('class' => 'btn btn-mini btn-info')) !!}
                                        {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'DELETE', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');",  'route' => array(config('quickadmin.route').'.ustad.destroy', $row->id))) !!}
                                        {!! Form::submit(trans('quickadmin::templates.templates-view_index-delete'), array('class' => 'btn btn-mini btn-danger')) !!}
                                        {!! Form::close() !!}
                                        @if($row->is_active == 0)
                                            {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');", 'action' => 'Admin\UstadController@blackList')) !!}
                                            <input type="hidden" value="{{ $row->id }}" name="id">
                                            <input type="hidden" value="1" name="is_active">
                                            <button type="submit" class="btn btn-mini btn-secondary">Deactive</button>
                                            {!! Form::close() !!}
                                        @else
                                            {!! Form::open(array('style' => 'display: inline-block;', 'method' => 'POST', 'onsubmit' => "return confirm('".trans("quickadmin::templates.templates-view_index-are_you_sure")."');", 'action' => 'Admin\UstadController@blackList')) !!}
                                            <input type="hidden" value="{{ $row->id }}" name="id">
                                            <input type="hidden" value="0" name="is_active">
                                            <button type="submit" class="btn btn-mini btn-warning">Active</button>
                                            {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <button class="btn btn-sm btn-danger" id="delete">
                                {{ trans('quickadmin::templates.templates-view_index-delete_checked') }}
                            </button>
                        </div>
                    </div>
                    {!! Form::open(['route' => config('quickadmin.route').'.ustad.massDelete', 'method' => 'post', 'id' => 'massDelete']) !!}
                    <input type="hidden" id="send" name="toDelete">
                    {!! Form::close() !!}

            </div>

            @else
                {{ trans('quickadmin::templates.templates-view_index-no_entries_found') }}
            @endif
        </div>

    </div>
    </div>
    </div>
    <div class="row">

    </div>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {
            $('#delete').click(function () {
                if (window.confirm('{{ trans('quickadmin::templates.templates-view_index-are_you_sure') }}')) {
                    var send = $('#send');
                    var mass = $('.mass').is(":checked");
                    if (mass == true) {
                        send.val('mass');
                    } else {
                        var toDelete = [];
                        $('.single').each(function () {
                            if ($(this).is(":checked")) {
                                toDelete.push($(this).data('id'));
                            }
                        });
                        send.val(JSON.stringify(toDelete));
                    }
                    $('#massDelete').submit();
                }
            });

            ({
                date: false,
                shortTime: false,
                format: 'HH:mm'
            });
        });

        $(document).ready(function() {

            $('#openBtn').click(function() {
                $('#myModal').modal({
                    show: true
                })
            });

            $(document).on({
                'show.bs.modal': function() {
                    var zIndex = 1040 + (10 * $('.modal:visible').length);
                    $(this).css('z-index', zIndex);
                    setTimeout(function() {
                        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
                    }, 0);
                },
                'hidden.bs.modal': function() {
                    if ($('.modal:visible').length > 0) {
                        // restore the modal-open class to the body element, so that scrolling works
                        // properly after de-stacking a modal.
                        setTimeout(function() {
                            $(document.body).addClass('modal-open');
                        }, 0);
                    }
                }
            }, '.modal');
        });

    </script>


    <script type="text/javascript" src="{{ URL::asset('js/timedropper.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/select2/js/select2.full.min.js')}}"></script>
    <!-- Multiselect js -->
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/bootstrap-multiselect/js/bootstrap-multiselect.js')}}">
    </script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//bower_components/multiselect/js/jquery.multi-select.js')}}"></script>
    <script type="text/javascript" src="{{ URL::asset('ablepro//assets/js/jquery.quicksearch.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{ URL::asset('ablepro/assets/pages/advance-elements/select2-custom.js')}}"></script>
    <script>
        @foreach ($ustad as $row2)
        $( "#start_time{{$row2->id}}" ).timeDropper()
        $( "#end_time{{$row2->id}}" ).timeDropper()
        @endforeach
    </script>

@endsection