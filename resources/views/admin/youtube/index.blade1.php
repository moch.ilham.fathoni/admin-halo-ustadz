@extends('admin.layouts.master')
@section('content')
    <div class="col-sm-12">
        <!-- Nestable card start -->
        <div class="card">
            <div class="card-header">
                <h5>Youtube</h5>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-md-12 col-sm-offset-2">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    {!! implode('', $errors->all('
                                    <li class="error">:message</li>
                                    ')) !!}
                                </ul>
                            </div>
                        @endif
                    </div>
                </div>

                {!! Form::open(['route' => ['youtube.update', $youtube->id], 'class' => 'form-horizontal', 'method' => 'PATCH']) !!}

                <div class="form-group">
                    {!! Form::label('title', 'Title', ['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-md-12">
                        {!! Form::text('title', old('title', $youtube->title), ['class'=>'form-control', 'placeholder'=> 'Title']) !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('Url', 'Url', ['class'=>'col-sm-2 control-label']) !!}
                    <div class="col-md-12">
                        {!! Form::text('url', old('url', $youtube->url), ['class'=>'form-control', 'placeholder'=> 'Url']) !!}
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-md-12 col-sm-offset-2">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection