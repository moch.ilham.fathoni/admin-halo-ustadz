<?php

use App\Http\Controllers\Admin\ArtikelController;
use App\Http\Controllers\Admin\DoaController;
use App\Http\Controllers\Admin\DoaHarianController;
use App\Http\Controllers\Admin\EventController;
use App\Http\Controllers\Admin\HaditsController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Auth::routes();
Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LoginController::class, 'logout'])->name('logout');



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/dashboard',[App\Http\Controllers\Admin\DashboardController::class, 'index']);

Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard',[App\Http\Controllers\Admin\DashboardController::class, 'index']);
    Route::resource('/doa', 'App\Http\Controllers\Admin\DoaController');
    Route::prefix('/content')->name('content.')->group(function(){
        Route::resource('/artikel', ArtikelController::class);
        Route::post('/categorydoa/create',[DoaController::class, 'createcategorydoa']);
        Route::post('/doa/create',[DoaController::class, 'createdoa']);
        Route::delete('/doa/{id}',[DoaController::class, 'delete']);
        Route::resource('/doaharian', DoaHarianController::class);
        Route::resource('/hadits', HaditsController::class);
        Route::resource('/event', EventController::class);
    });

    Route::DELETE('/doaharian/{id}/delete',[App\Http\Controllers\Admin\DoaHarianController::class],'destroy')->name('admin.doaharian.destroy');
    Route::post('',[App\Http\Controllers\Admin\DoaController::class],'massDelete')->name('doaharian.artikel.massDelete');
    // Route::resource('/Admin/doaharian',[App\Http\Controllers\Admin\DoaHarianController::class, 'store']);
});


